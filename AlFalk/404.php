<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package radix
 * @since radix 1.0
 *
 */
get_header(); ?>



<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            404</h3>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">  
    	<div id="primary" class="col-12 content-area">
    		<section class="error-404 not-found">
    			<div class="page-content">
					<div class="row">
						<div class="col-12 error-404-box">
							<div class="error-message">404</div>
							<p class="lead"><?php _e('Ooops! Page not found!','radix'); ?></p>
							<p><?php _e('Try searching for the best match or visit our home page','radix'); ?></p>
							<a href="<?php echo esc_url( home_url( '/' )); ?>" class="return btn ">
							    <?php _e('Go to the home page','radix'); ?>
							</a>
						</div>
					</div>
    			</div>
    		</section>
    	</div>
    </div>
</div>
<?php get_footer(); ?>
