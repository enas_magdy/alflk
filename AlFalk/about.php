<?php
/**
 * Template Name: about
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
get_header(); 
?>

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php the_title(); ?></h3>
        </div>
    </div>
</div>


<div class="about-page">
    <div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php echo the_content(); ?>
    <?php endwhile; ?>
    <?php endif; ?>
    
    <?php if(get_field("about_content")){ ?>
    <div class="about_content">
        <h3 class="main_title"><?php the_title(); ?></h3>
        <div class="text">
            <p><?php the_field("about_content"); ?></p>
        </div>
    </div>
    <?php } ?>
    
    <?php if(get_field("vision")) { ?>
    <div class="vision">
        <div class="d-flex">
            <div class="image">
                <img src="<?php the_field("vision_img"); ?>" />
            </div>
            <div class="text">
                <h3 class="main_title"><?php _e("Vision", "radix"); ?></h3>
                <p><?php the_field("vision"); ?></p>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if(get_field("message")) { ?>
        <div class="message">
            <div class="d-flex">
                <div class="text">
                    <h3 class="main_title"><?php _e("Message", "radix"); ?></h3>
                    <p><?php the_field("message"); ?></p>
                </div>
                <div class="image">
                    <img src="<?php the_field("message_img"); ?>" />
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</div>

<?php get_template_part( 'templates/content', 'vision'); ?>
<?php get_template_part( 'templates/content', 'newsletter'); ?>



<?php get_footer(''); ?>