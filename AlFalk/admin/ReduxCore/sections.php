<?php
/**
 * @package radix
 * @since radix 1.0
 */

/* General */
$this->sections[] = array(
    'icon'      => 'el el-icon-cog',
    'title'     => __('General', 'radix'),
    'fields'    => array(
        array(
            'id'    => 'info_success',
            'type'  => 'info',
            'style' => 'success',
            'title' => __('Welcome to radix Theme Option Panel', 'radix'),
            'icon'  => 'el el-icon-smiley',
            'desc'  => __( 'From here you can config your theme in the way you like.', 'radix')
            ),
        array(
            'id' => 'favicon',
            'type' => 'media',
            'url' => true,
            'title' => __('Favicon', 'radix'), 
            'subtitle' => __('Upload your favicon here', 'radix'),
            'desc' => __('Should be 16 x 16px Supported formats: .ico .gif .png', 'radix'),
            'default'   => ''
            ),

        array(
            'id' => 'apple_touch_icon',
            'type' => 'media',
            'url' => true,
            'title' => __('Apple Touch Icon', 'radix'), 
            'subtitle' => __('Upload icon for the Apple touch', 'radix'),
            'desc' => __('Size: 57 x 57px Supported formats: .ico .gif .png', 'radix'),
            'default'   => ''
            ),

        array(
            'id'        => 'scroll_to_top',
            'type'      => 'switch',
            'title'     => __('Scroll to top button', 'radix'),
            'subtitle'  => __('Check if you want to disable scroll to top button', 'radix'),
            'default'   => true
            ),
    )
);

/* Header */
$this->sections[] = array(
     'icon'      => 'el el-icon-user',
    'title'     => __('أعلى الصفحة', 'radix'),
    'desc'      => __('These are options to modify and style your header area', 'radix'),
    'fields'    => array(
        array(
            'id'        => 'logo',
            'type'      => 'media',
            'url'       => true,
            'title'     => __('الشعار', 'radix'),
            'subtitle'  => __('اتركه فارغا وسيظهر اسم الموقع بدلا من الشعار', 'radix'),
            'desc'      => __(' .jpg, .png and .gif', 'radix'),
            'default'   => ''
            ),
    )
);


$this->sections[] = array(
   'icon'      => 'el el-icon-screen',
    'title'     => __('التواصل الاجتماعى', 'radix'),
    'desc'      => __('These are options to modify and style your body area', 'radix'),
    'fields'    => array(
            array(
                'id'        =>'facebook',
                'type'      => 'text',      
                'title'     => __('Facebook', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),
             array(
                'id'        =>'google',
                'type'      => 'text',      
                'title'     => __('google', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),
            array(
                'id'        =>'twitter',
                'type'      => 'text',      
                'title'     => __('Twitter', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),
            array(
                'id'        =>'instagram',
                'type'      => 'text',      
                'title'     => __('Instagram', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),
            array(
                'id'        =>'youtube',
                'type'      => 'text',      
                'title'     => __('Youtube', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),
            array(
                'id'        =>'linkedin',
                'type'      => 'text',      
                'title'     => __('linkedin', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),
            array(
                'id'        =>'snapchat',
                'type'      => 'text',      
                'title'     => __('snapchat', 'radix'), 
                'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
                'desc'      => "",
                'default'   => "#"
                ),   
         )
    );

/* Body */
$this->sections[] = array(
   'icon'      => 'el el-icon-screen',
    'title'     => __('محتوى الصفحة الرئيسية', 'radix'),
    'desc'      => __('These are options to modify and style your body area', 'radix'),
    'fields'    => array(
        array(
            'id'        => 'post_slider_section',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء السليدر الرئيسى</h4>'),
        ), 
            array(
                'id'        => 'slider_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض السليدر الرئيسي ', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
                array(
                    'id'        =>'slider_id',
                    'type'      => 'text',      
                    'title'     => __('رقم السليدر', 'radix'),
                    'desc'      => "",
                ),
                array(
                    'id'        =>'slider_id_en',
                    'type'      => 'text',      
                    'title'     => __('رقم السليدر باللغة الانجليزية', 'radix'),
                    'desc'      => "",
                ),
        array(
            'id'        => 'about_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء من نحن </h4>'),
        ),
            array(
                'id'        => 'about_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء من نحن ', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
            
            array( 
                'id' => 'about',
                'type' => 'repeater', 
                'title' => __( 'محتوي من نحن ', 'tt' ), 
                'sortable' => true, 
                'static' => '2',
                'fields' => array(
                    array(
                        'id'        =>'about_title',
                        'type'      => 'text',      
                        'title'     => __('العنوان', 'radix'),
                        'desc'      => "",
                    ),
                    array(
                        'id'        =>'about_title_en',
                        'type'      => 'text',      
                        'title'     => __('العنوان باللغة الانجليزية', 'radix'),
                        'desc'      => "",
                    ),
                    array(
                        'id'        =>'about_content',
                        'type'      => 'textarea',      
                        'title'     => __('المحتوي', 'radix'),
                        'desc'      => "",
                        'args'   => array(
                            'teeny'            => true,
                            'textarea_rows'    => 10
                        )
                    ),
                    array(
                        'id'        =>'about_content_en',
                        'type'      => 'textarea',      
                        'title'     => __('المحتوي  باللغة الانجليزية', 'radix'),
                        'desc'      => "",
                        'args'   => array(
                            'teeny'            => true,
                            'textarea_rows'    => 10
                        )
                    ),
                    
                    array(
                        'id'        =>'about_img',
                        'type'      => 'media',      
                        'title'     => __('الصورة', 'radix'),
                        'url'       => true,
                        'desc'      => __(' .jpg, .png and .gif', 'radix'),
                    ),
                ),
            ),
        array(
            'id'        => 'products_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزءالمنتجات </h4>'),
        ),
            array(
                'id'        => 'products_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء البرامج', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
        array(
            'id'        => 'statistics_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء الاحصائيات </h4>'),
        ),
            array(
                'id'        => 'statistics_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء الاحصائيات', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
            array(
                'id'        =>'statistics_image',
                'type'      => 'media',      
                'title'     => __('الصورة', 'radix'),
                'url'       => true,
                'desc'      => __(' .jpg, .png and .gif', 'radix'),
            ),
            array( 
                'id' => 'statistics',
                'type' => 'repeater', 
                'title' => __( 'الاحصائيات', 'tt' ), 
                'sortable' => true, 
                'static' => '2',
                'fields' => array(
                    array(
                        'id'        =>'statistics_title',
                        'type'      => 'text',      
                        'title'     => __('العنوان', 'radix'),
                        'desc'      => "",
                    ),
                    array(
                        'id'        =>'statistics_title_en',
                        'type'      => 'text',      
                        'title'     => __('العنوان باللغة الانجليزية', 'radix'),
                        'desc'      => "",
                    ),
                    array(
                        'id'        =>'statistics_content',
                        'type'      => 'text',      
                        'title'     => __('العدد', 'radix'),
                        'desc'      => "",
                    ),
                    array(
                        'id'        =>'statistics_img',
                        'type'      => 'media',      
                        'title'     => __('الصورة', 'radix'),
                        'url'       => true,
                        'desc'      => __(' .jpg, .png and .gif', 'radix'),
                    ),  
                ),
            ),  
        array(
            'id'        => 'projects_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء المشاريع  </h4>'),
        ),
            array(
                'id'        => 'projects_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء المشاريع', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),    
        array(
            'id'        => 'partners_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء شركاء النجاح </h4>'),
        ),
            array(
                'id'        => 'partners_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء شركاء النجاح', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
        array(
            'id'        => 'news_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء الاخبار </h4>'),
        ),
            array(
                'id'        => 'news_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء الاخبار', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
        array(
            'id'        => 'vision_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء الرؤية   </h4>'),
        ),
            array(
                'id'        => 'vision_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء الرؤية  ', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ),
            array(
                'id'        =>'vision_content',
                'type'      => 'text',      
                'title'     => __('المحتوي', 'radix'),
                'desc'      => "",
            ),
            array(
                'id'        =>'vision_content_en',
                'type'      => 'text',      
                'title'     => __('المحتوي باللغة الانجليزية', 'radix'),
                'desc'      => "",
            ),
            array(
                'id'        =>'vision_img',
                'type'      => 'media',      
                'title'     => __('الصورة', 'radix'),
                'url'       => true,
                'desc'      => __(' .jpg, .png and .gif', 'radix'),
            ), 
        array(
            'id'        => 'newsletter_section_title',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">جزء النشرة البريدية  </h4>'),
        ),
            array(
                'id'        => 'newsletter_section',
                'type'      => 'switch',
                'multi'    => false,
                'title'     => __('عرض جزء النشرة البريدية ', 'radix'),
                'options'  => array(
                    '1' => 'أظهار',
                    '2' => 'إخفاء',
                ),
                'default'  => '1',
            ), 
    )
);


/* Footer */
$this->sections[] = array(
    'icon'      => 'el el-icon-bookmark',
    'title'     => __('أسفل الصفحة', 'radix'),
    'desc'      => __('Manage settings for footer area', 'radix'),
    'fields'    => array(
        array(
            'id'        => 'footer_display',
            'type'      => 'switch',
            'switch'    => true,
            'title'     => __('Enable Footer Widgets', 'radix'),
            'subtitle'  => __('Check if you want to disable footer widgets area.', 'radix'),
            'default'   => true
            ),
        array(
            'id'        => 'footer_col',
            'type'      => 'image_select',
            'compiler'  => true,
            'title'     => __('Footer Widget Columns', 'radix'),
            'subtitle'  => __('Choose columns you want for your footer widgets.', 'radix'),
            'desc'      => __('You can manage footer area content in <a href="' . admin_url('widgets.php') . '">Apperance / Widgets</a> settings.', 'radix'),
            'options'   => array(
                '1' => array('alt' => '1 Column ', 'img' => radix_IMG_URI . '/1column.png'),
                '2' => array('alt' => '2 Column ', 'img' => radix_IMG_URI . '/2columns.png'),
                '3' => array('alt' => '3 Column ', 'img' => radix_IMG_URI . '/3columns.png'),
                '4' => array('alt' => '4 Column ', 'img' => radix_IMG_URI . '/4columns.png')
                ),
            'required'  => array('footer_display', '=', true),
            'default'   => '4',
            
            ),
         array(
            'id'        => 'enable_footer_social',
            'type'      => 'switch',
            'title'     => __('Enable Footer Socials', 'radix'),
            'subtitle'  => __('Check if you want to hide footer socials bar','radix'),
            'default'   => true
            ),
        array(
            'id'        =>'footer_socials_text',
            'type'      => 'text',      
            'title'     => __('Footer Social Text', 'radix'), 
            'subtitle'  => __('Change post social text here.', 'radix'),
            'required'  => array('footer_display', '=', true),
            'default'   => "Follow us: "          
            ),
         array(
            'id'        => 'enable_footer_menu',
            'type'      => 'switch',
            'title'     => __('Enable Footer Menu', 'radix'),
            'subtitle'  => __('Check if you want to show footer menu inside copyright area', 'radix'),
            'desc'  => __('Note: you need to set footer menu inside <a href="' . admin_url('nav-menus.php') . '">Apperance / Menu</a>', 'radix'),
            'default'   => true,
            ),
)
    
);

$this->sections[] = array(
    'icon'      => 'el el-icon-screen',
    'title'     => __('معلومات التواصل', 'radix'),
    'desc'      => __('These are options to modify and style your body area', 'radix'),
    'fields'    => array(
         array(
            'id'        =>'phone',
            'type'      => 'text',      
            'title'     => __('رقم الجوال', 'radix'),
            'desc'      => "",
        ),
        array(
            'id'        =>'whats',
            'type'      => 'text',      
            'title'     => __('رقم الواتساب', 'radix'), 
            'subtitle'  => __('اتركه فارغا لتخفيه.', 'radix'),
            'desc'      => "",
            'default'   => "#"
            ),
        array(
            'id'        =>'email',
            'type'      => 'text',      
            'title'     => __('البريد الالكترونى', 'radix'),
            'desc'      => "",
            ),
        array(
            'id'        =>'address',
            'type'      => 'textarea',      
            'title'     => __('العنوان  ', 'radix'),
            'desc'      => "",
            ),
        array(
            'id'        =>'address_en',
            'type'      => 'textarea',      
            'title'     => __('العنوان باللغة الاتجليزية  ', 'radix'),
            'desc'      => "",
            ),    
        array(
            'id'        =>'mapaddress',
            'type'      => 'textarea',      
            'title'     => __('العنوان على الخريطة ', 'radix'),
            'desc'      => "",
            ),  
        array(
            'id'        =>'mapaddress_en',
            'type'      => 'textarea',      
            'title'     => __('العنوان على الخريطة باللغة الانجليزية', 'radix'),
            'desc'      => "",
            ),      

    )
);



/* Advanced options */
$this->sections[] = array(
    'icon' => 'el el-icon-cogs',
    'icon_class' => 'el el-icon-large',
    'title' => __('Advanced Options', 'radix'),
    'desc'     => __('Advanced options for avanced users here', 'radix'),
    'fields' => array(
        array(
            'id'        => 'additional_codes',
            'icon'      => true,
            'type'      => 'info',
            'style'     => 'info',
            'raw'       => __('<h4 style="margin: 3px;">Custom Codes</h4>'),
            ),
        array(
            'id'       => 'custom_css',
            'type'     => 'ace_editor',
            'title'    => __('Additional CSS', 'radix'),
            'subtitle' => __('Use this field to write or paste CSS code and modify default theme styling', 'radix'),
            'mode'     => 'css',
            'theme'    => 'monokai',
            'default' => ".example {\nmargin: 0 auto;\n}"
            ),
        array(
            'id'       => 'custom_js',
            'type'     => 'ace_editor',
            'title'    => __('Additional JavaScript', 'radix'),
            'subtitle' => __('Use this field to write or paste additional JavaScript code to this theme', 'radix'),
            'mode'     => 'javascript',
            'theme'    => 'chrome',
            'default' => "(function($){\n/* standard on load code goes here with $ prefix */\n})(jQuery);"
            ),
    )
);

/* maintenance */
$this->sections[] = array(
    'icon'      => 'el el-icon-warning-sign',
    'title'     => __('Maintenance Mode', 'radix'),
    'desc'      => __('Basic maintenance mode', 'radix'),
    'fields'    => array(
        array(
            'id'        => 'maintenance_mode',
            'type'      => 'switch',
            'title'     => __('Maintenance mode', 'radix'),
            'subtitle'  => __('Enable maintenance mode.', 'radix'),
             'desc'  => __('Check if you want to enable the default wordpress maintenance mode.', 'radix'),
            'default'   => false,
            ),
        )
);


?>