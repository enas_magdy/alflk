<?php 

/**
* @package radix enqueued scripts
* @since radix 1.0
*
*/

/**
    Load needed scripts
**/

function radix_scripts() {

	wp_enqueue_script('radix-modernizr',      radix_JS_URI . '/modernizr.min.js', 	array(), '2.8.3', true);
	wp_enqueue_script('popper.min.js',    radix_JS_URI . '/popper.min.js',array('jquery'), '', true);
	wp_enqueue_script('radix-bootstrapjs',    radix_JS_URI . '/bootstrap.min.js', 	array('jquery'), '3.3.5', true);
	// jquery.nicescroll + WOW.js
	wp_enqueue_script('radix-plugins',        radix_JS_URI . '/plugins.min.js', 	array(), '1.1.2', true);
	wp_enqueue_script('radix-scripts',   	  radix_JS_URI . '/scripts.min.js', 	array(), '1.1.2', true);
	wp_enqueue_script('radix-demo6',   	  radix_JS_URI . '/demo6.js', 	array(), '1.1.2', true);
	wp_enqueue_script('fancy',   	  radix_JS_URI . '/jquery.fancybox.min.js', 	array(), '1.1.2', true);
	wp_enqueue_script('slick-',   	  radix_JS_URI . '/slick.min.js', 	array());
	wp_enqueue_script('select2-',   	  radix_JS_URI . '/select2.min.js', 	array());
	wp_enqueue_script('swiperjs',   	  radix_JS_URI . '/swiper-bundle.min.js', 	array());
	wp_enqueue_script('sweetalert2',   	 'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.0/sweetalert2.min.js', 	array());

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script('keyboard-image-navigation',   radix_JS_URI . '/keyboard-image-navigation.js');
	}

	if ( (!is_admin()) && is_singular() && comments_open() && !is_front_page() && get_option('thread_comments')) { 
		wp_enqueue_script( 'comment-reply' ); 
	}

	// Animate.css - font-awesome
	wp_enqueue_style('radix-all-css',   		radix_CSS_URI 	. '/all.min.css');
	// Load Bootstrap
	wp_enqueue_style('radix-bootstrap', 		radix_CSS_URI   . '/bootstrap.min.css');
	// Load main stylesheet
    wp_enqueue_style('NeoSansArabic',     radix_CSS_URI 	. '/NeoSansArabic/stylesheet.css');
    wp_enqueue_style('Alexandria',     radix_CSS_URI 	. '/Alexandria/stylesheet.css');
    wp_enqueue_style('Bahij',     radix_CSS_URI 	. '/Bahij/stylesheet.css');
    wp_enqueue_style('Korolev',     radix_CSS_URI 	. '/Korolev/stylesheet.css');
    wp_enqueue_style('flaticon',     radix_CSS_URI 	. '/flaticon/flaticon.css');
    wp_enqueue_style('slick',     radix_CSS_URI 	. '/animate.css');
    wp_enqueue_style('animate',     radix_CSS_URI 	. '/slick.css');
    wp_enqueue_style('select2',     radix_CSS_URI 	. '/select2.min.css');
    wp_enqueue_style('slick-theme',     radix_CSS_URI 	. '/slick-theme.css');
    wp_enqueue_style('swipercss',     radix_CSS_URI 	. '/swiper-bundle.min.css');
    wp_enqueue_style('fancy-css',     radix_CSS_URI 	. '/jquery.fancybox.min.css');
    wp_enqueue_style('sweetalert2',    'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.0/sweetalert2.min.css');
	// Load main stylesheet
	wp_enqueue_style( 'radix-style', get_stylesheet_uri() );

	if(is_rtl())
	{
	    	wp_enqueue_style('radix-bootstrap-rtl', 		radix_CSS_URI   . '/bootstrap-rtl.min.css');
	        wp_enqueue_style('custom-css',     radix_CSS_URI 	. '/stylesheet.css');
	}
	else
	{
	        wp_enqueue_style('custom-css',     radix_CSS_URI 	. '/stylesheet_en.css');

	}
	wp_enqueue_style( 'radix-style', get_stylesheet_uri() );
}

add_action('wp_enqueue_scripts', 'radix_scripts');


function radix_IE() {
	global $wp_scripts;
	wp_enqueue_script( 'radix-html5shiv', 		radix_JS_URI 	. '/html5shiv.js', array(), '3.7.2', false );
	$wp_scripts->add_data( 'radix-html5shiv', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'radix_IE' );
