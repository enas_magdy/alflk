<?php
/**
 * The template for displaying archive pages.
 *
 * @package radix
 * @since radix 1.0
 */

get_header(); ?>


<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php _e("Products","radix") ?></h3>
        </div>
    </div>
</div>

<div class="products-page products">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between tax_items">
            <h3 class="main_title"><?php _e("Products","radix") ?>
            </h3>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tab-all" data-toggle="tab" href="#tabcontent-all" role="tab" aria-controls="home" aria-selected="true"><?php _e("all", "radix"); ?></a>
                </li>
                <?php
                    $terms = get_terms('products_category',array(
                        'hide_empty' => false,
                    ) );
                    foreach($terms as $term) {
                ?>
                  <li class="nav-item">
                    <a class="nav-link" id="tab-<?php echo $term->term_id; ?>" data-toggle="tab" href="#tabcontent-<?php echo $term->term_id; ?>" role="tab" aria-controls="home" aria-selected="true"><?php echo $term->name; ?></a>
                  </li>
                <?php } ?>
                
            </ul>

        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="tabcontent-all" role="tabpanel" aria-labelledby="all-tab">
                <div class="archive row no-gutters">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    	<?php get_template_part( 'loop/content', get_post_format()); ?>
                    <?php endwhile; endif; ?>
                </div>
            </div>

            <?php
                $terms = get_terms('products_category',array(
                    'hide_empty' => false,
                ) );
                foreach($terms as $term) {
            ?>
                <div class="tab-pane fade" id="tabcontent-<?php echo $term->term_id; ?>" role="tabpanel" aria-labelledby="tab-<?php echo $term->term_id; ?>">
                    <div class="archive row no-gutters">

                    <?php
                       $query = new WP_Query( array(
                               'post_type'     => 'products', //your post type
                               'posts_per_page' => -1,
                               'order' => 'ASC',
                               'tax_query' => array(
                                array(
                                    'taxonomy' => 'products_category',
                                    'field'    => 'term_id',
                                    'terms'    => $term
                                )
                            )
                           )
                       );
                   
                       while ($query->have_posts()) {
                           $query->the_post();
                           //whatever code you want
                    ?> 
                        <?php get_template_part( 'loop/content', get_post_format()); ?>

                    <?php } ?>
                </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<?php get_footer(); ?>