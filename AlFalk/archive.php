<?php
/**
 * The template for displaying archive pages.
 *
 * @package radix
 * @since radix 1.0
 */

if (  'partners'  == get_post_type() ) {
    $class ="partners partners-page" ;
} 


if (  'post'  == get_post_type() ) {
    $class ="news news-page" ;
} 

if (  'products'  == get_post_type() ) {
    $class ="products-page products";
} 

if (  'projects'  == get_post_type() ) {
    $class ="projects-page projects";
} 

if (  'bank_accounts'  == get_post_type() ) {
    $class ="bank_accounts-page";
} 

if (  'governance'  == get_post_type() ) {
    $class ="news news-page governance-page";
} 


get_header(); ?>


<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php if (  'projects'  == get_post_type() ) { ?><?php _e("Projects","radix") ?><?php } ?> 
            <?php if (  'bank_accounts'  == get_post_type() ) { ?><?php _e("Bank Accounts","radix") ?><?php } ?> 
            <?php if (  'products'  == get_post_type() ) { ?> <?php _e("Products","radix") ?>
            <?php } ?>
            <?php if (  'partners'  == get_post_type() ) { ?> <?php _e("Partners","radix") ?>
            <?php } ?>
            <?php if (  'post'  == get_post_type() ) { ?><?php _e("News","radix") ?>
            <?php } ?> 
            <?php if (  'governance'  == get_post_type() ) { ?><?php _e("governance","radix") ?>
            <?php } ?> </h3>
        </div>
    </div>
</div>

<div class="<?php echo $class; ?>">
    <div class="container">
        <h3 class="main_title"><?php if (  'projects'  == get_post_type() ) { ?><?php _e("Projects","radix") ?><?php } ?> 
            <?php if (  'bank_accounts'  == get_post_type() ) { ?><?php _e("Bank Accounts","radix") ?><?php } ?> 
            <?php if (  'products'  == get_post_type() ) { ?> <?php _e("Products","radix") ?>
            <?php } ?>
            <?php if (  'partners'  == get_post_type() ) { ?> <?php _e("Partners","radix") ?>
            <?php } ?>
            <?php if (  'post'  == get_post_type() ) { ?><?php _e("News","radix") ?>
            <?php } ?> 
            <?php if (  'governance'  == get_post_type() ) { ?><?php _e("governance","radix") ?>
            <?php } ?></h3>
        <div class="archive row no-gutters">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            	<?php get_template_part( 'loop/content', get_post_format()); ?>
            <?php endwhile; ?>
            <?php else : ?>
            	<?php get_template_part( 'loop/content', 'none' ); ?>
            <?php endif; ?>
        </div>
    </div>
</div>


<?php get_footer(); ?>