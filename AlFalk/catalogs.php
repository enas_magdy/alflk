<?php 

/**
 * Template Name: catalogs-page
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
 
 
get_header(); ?>



<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / <?php _e("Catalogs","radix") ?></h3>
        </div>
    </div>
    <div class="image">
        <img src="<?php echo get_template_directory_uri(); ?>/includes/images/breadcrumb.png" />
    </div>
</div>


<div class="news services-page">
    <div class="container">
        <!--<div class="title mb-5 pb-5">-->
        <!--    <h3 class="wow fadeInRight" data-wow-delay="0.1s"><?php _e('Catalogs',"radix"); ?></h3>-->
        <!--    <h4 class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Boulevard","radix"); ?></h4>-->
        <!--</div>-->
    		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <?php

               $query = new WP_Query( array(
                       'post_type'     => 'catalogs', //your post type
                       'posts_per_page' => 9,
                        'paged' => $paged,
                   )
               );
               if ( have_posts() ) {
           
               while ($query->have_posts()) {
                   $query->the_post();
                   //whatever code you want
            ?>     
            <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4 col-12">
                <a href="<?php echo get_field("file");?>" target="_blank">
                    <div class="item">
        			    <div class="image text-center">
                            <img src="<?php the_post_thumbnail_url('image');  ?>" />
                        </div>
                        <div class="caption">
                            <h3><?php echo the_title(); ?></h3>
                        </div>
            		</div>   		
            	</a>
    		</div>

    		<?php } } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
