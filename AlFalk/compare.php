<?php
/**
 * Template Name: compare
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */

get_header(); 

?>


<div class="container">
   <div id="breadcrumbs">
        <div class="row">
            <h3 class=""><?php the_title(); ?></h3>
        </div>
    </div>
    <div class="cars-slider cars offers-page">
    <div class="row">

<?php
$COMPARISON_QUERY = new WP_Query(array(
    'post_type' => array('cars'),
    'post__in' => getComparedItems(),
));
if(getComparedItems()) {
while ($COMPARISON_QUERY->have_posts()) {
               $COMPARISON_QUERY->the_post();
            $price = get_field("service_price");
?>
        <div class="col-lg-4 col-xl-4 col-md-4 col-sm-4 col-12" id="itemCompare-<?php echo get_the_ID(); ?>">

            <div class="item">
				<div class="image text-center">
				    <a href="<?php echo get_permalink();?>">
                        <img src="<?php the_post_thumbnail_url('image');  ?>" />
                    </a>
                    <div class="favorite">
                        <?php echo do_shortcode("[favorite_button post_id='' site_id='']"); ?>
                        <!--<svg xmlns="http://www.w3.org/2000/svg" width="49.604" height="44.361" viewBox="0 0 49.604 44.361">-->
                        <!--  <path id="Vector" d="M25.543,42.6a5.245,5.245,0,0,1-2.982,0C15.586,40.215,0,30.282,0,13.445A13.406,13.406,0,0,1,13.373,0,13.266,13.266,0,0,1,24.052,5.388,13.341,13.341,0,0,1,48.1,13.445C48.1,30.282,32.518,40.215,25.543,42.6Z" transform="translate(0.75 0.75)" fill="none" stroke="#a50014" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>-->
                        <!--</svg>-->
                    </div>
                </div>
                <div class="caption">
					<h4><?php echo the_title(); ?></h4>
					<div class="price-card d-flex align-items-center justify-content-between">
					    <p class="discount_price"><?php echo the_field("discount_price"); ?></p>
					    <p class="price"><?php echo the_field("price"); ?></p>
					</div>
					<ul class="car_details">
					    <?php if(get_field("engine_capacity")) { ?>
    					    <li class="d-flex align-items-center justify-content-between">
    					        <span><?php _e("Engine Capacity") ?></span>
    					        <span><?php echo the_field("engine_capacity") ?></span>
    					    </li>
					    <?php } if(get_field("driveـtype")) { ?>
    					    <li class="d-flex align-items-center justify-content-between">
    					        <span><?php _e("Drive Type") ?></span>
    					        <span><?php echo the_field("driveـtype"); ?></span>
    					    </li>
					    <?php } if(get_field("release_year")) { ?>
    					    <li class="d-flex align-items-center justify-content-between">
    					        <span><?php _e("Release Year") ?></span>
    					        <span><?php echo the_field("release_year") ?></span>
    					    </li>
					    <?php } if(get_field("engine_type")) { ?>
    					    <li class="d-flex align-items-center justify-content-between">
    					        <span><?php _e("Engine Type") ?></span>
    					        <span><?php echo the_field("engine_type") ?></span>
    					    </li>
    					<?php } ?>
					</ul>
					<ul class="list-inline options">
					    <li class="list-inline-item">
					        <a href="<?php echo get_permalink();?>">
					            <svg id="eye" xmlns="http://www.w3.org/2000/svg" width="39.842" height="39.842" viewBox="0 0 39.842 39.842">
                                  <path id="Vector" d="M11.886,5.943A5.943,5.943,0,1,1,5.943,0,5.937,5.937,0,0,1,11.886,5.943Z" transform="translate(13.978 13.978)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                  <path id="Vector-2" data-name="Vector" d="M16.244,27.474c5.86,0,11.322-3.453,15.123-9.429a8.841,8.841,0,0,0,0-8.616C27.566,3.453,22.1,0,16.244,0S4.922,3.453,1.121,9.429a8.841,8.841,0,0,0,0,8.616C4.922,24.021,10.384,27.474,16.244,27.474Z" transform="translate(3.677 6.175)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                  <path id="Vector-3" data-name="Vector" d="M0,0H39.842V39.842H0Z" transform="translate(39.842 39.842) rotate(180)" fill="none" opacity="0"/>
                                </svg>
                                <p><?php _e("view"); ?></p>
					        </a>
					    </li>
					    <li class="list-inline-item">
					        <a  class="compareRemove" data-id="<?php echo get_the_ID(); ?>">
					            <svg xmlns="http://www.w3.org/2000/svg" width="45.284" height="45.284" viewBox="0 0 45.284 45.284">
                                  <g id="vuesax_linear_flash" data-name="vuesax/linear/flash" transform="translate(-428 -316)">
                                    <g id="flash" transform="translate(428 316)">
                                      <path id="Vector" d="M2.582,21.285h5.83V34.87c0,3.17,1.717,3.811,3.811,1.434L26.507,20.077c1.755-1.981,1.019-3.623-1.642-3.623h-5.83V2.87c0-3.17-1.717-3.811-3.811-1.434L.94,17.662C-.8,19.662-.06,21.285,2.582,21.285Z" transform="translate(8.909 3.772)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                      <path id="Vector-2" data-name="Vector" d="M0,0H45.284V45.284H0Z" fill="none" opacity="0"/>
                                    </g>
                                  </g>
                                </svg>
                                <p><?php _e("remove"); ?></p>
					        </a>
					    </li>
					    <li class="list-inline-item">
					        <a href="<?php echo get_permalink();?>">
					            <svg id="gallery" xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                                  <path id="Vector" d="M13.417,38.333h11.5c9.583,0,13.417-3.833,13.417-13.417v-11.5C38.333,3.833,34.5,0,24.917,0h-11.5C3.833,0,0,3.833,0,13.417v11.5C0,34.5,3.833,38.333,13.417,38.333Z" transform="translate(3.833 3.833)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                  <path id="Vector-2" data-name="Vector" d="M7.667,3.833A3.833,3.833,0,1,1,3.833,0,3.833,3.833,0,0,1,7.667,3.833Z" transform="translate(13.417 11.5)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                  <path id="Vector-3" data-name="Vector" d="M0,13.326,9.449,6.981a4.318,4.318,0,0,1,5.06.268l.633.556a4.273,4.273,0,0,0,5.405,0L28.52.963a4.273,4.273,0,0,1,5.405,0l3.124,2.683" transform="translate(5.118 22.995)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                  <path id="Vector-4" data-name="Vector" d="M0,0H46V46H0Z" fill="none" opacity="0"/>
                                </svg>
                                <p><?php _e("images"); ?></p>
					        </a>
					    </li>
					</ul>
                </div>
    		</div>
    	</div>
<?php } } ?>
</div>
</div>
</div>

<script>
$ = jQuery;

$(document).ready(function() {
    $("#input_22_8").on("change", function() {
        var id = $("#input_22_8").val();
        console.log(id);
        var price = $price;
        console.log(price);
        $('.ginput_container_total').val(price);

          
    });
})
</script>
<?php

get_footer(''); ?>