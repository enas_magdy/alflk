<?php
/**
 * Template Name: contact
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */

get_header(); 

    global $phone;
    global $address;
    global $address_en;
    global $email;
    global $mapaddress;
    global $mapaddress_en;
?>


<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php the_title(); ?></h3>
        </div>
    </div>
</div>


<div class="contact-page">
    <div class="container">
        <h3 class="main_title"><?php the_title(); ?></h3>
        <div class="row">
            <div class="col-12 col-lg-12 col-sm-12 col-xl-12">
                <div class="contact-info">
                    <div class="d-flex justify-content-between">
                        <div class="item">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="51.749" height="51.827" viewBox="0 0 51.749 51.827">
                                  <path id="Path_3694" data-name="Path 3694" d="M53.749,44.317a6.6,6.6,0,0,1-.648,2.825,10.692,10.692,0,0,1-1.762,2.643,11.683,11.683,0,0,1-4.25,3.058,13.013,13.013,0,0,1-5.053.985,21.6,21.6,0,0,1-8.448-1.892A45.515,45.515,0,0,1,24.674,46.8a74.5,74.5,0,0,1-8.5-7.256,73.631,73.631,0,0,1-7.23-8.474,46.2,46.2,0,0,1-5.079-8.837A21.826,21.826,0,0,1,2,13.765a13.405,13.405,0,0,1,.933-5,11.927,11.927,0,0,1,2.98-4.328A7.593,7.593,0,0,1,11.3,2a4.87,4.87,0,0,1,2.1.466,4.224,4.224,0,0,1,1.736,1.451l6.012,8.474a8.868,8.868,0,0,1,1.037,1.814,4.1,4.1,0,0,1,.363,1.581,3.518,3.518,0,0,1-.544,1.84,8.814,8.814,0,0,1-1.451,1.84l-1.969,2.047a1.387,1.387,0,0,0-.415,1.037,2.353,2.353,0,0,0,.078.6c.078.207.155.363.207.518a21.5,21.5,0,0,0,2.41,3.317c1.166,1.348,2.41,2.721,3.757,4.094,1.4,1.373,2.747,2.643,4.12,3.809a19.888,19.888,0,0,0,3.343,2.384c.13.052.285.13.466.207a1.789,1.789,0,0,0,.648.1,1.426,1.426,0,0,0,1.062-.441L36.232,35.2A7.951,7.951,0,0,1,38.1,33.744a3.451,3.451,0,0,1,1.84-.544,4.135,4.135,0,0,1,1.581.337,10.022,10.022,0,0,1,1.814,1.011l8.577,6.09A3.931,3.931,0,0,1,53.335,42.3,5.316,5.316,0,0,1,53.749,44.317Z" transform="translate(-2 -2)" fill="#fff"/>
                                </svg>

                            </div>
                            <h5><?php _e("PhoneNumber","radix"); ?></h5>
                            <a href="tel:<?php echo $phone; ?>"><p><?php echo $phone; ?></p></a>
                            
                        </div>
                        <div class="item">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="47" height="49" viewBox="0 0 47 49">
                                  <defs>
                                    <clipPath id="clip-path">
                                      <rect id="Rectangle_65" data-name="Rectangle 65" width="47" height="49" transform="translate(-0.491 -0.185)" fill="#fff"/>
                                    </clipPath>
                                  </defs>
                                  <g id="Group_73571" data-name="Group 73571" transform="translate(0.491 0.185)">
                                    <g id="Group_73545" data-name="Group 73545" transform="translate(0 0)" clip-path="url(#clip-path)">
                                      <path id="Path_3679" data-name="Path 3679" d="M10.138,55.49a1.988,1.988,0,1,0,1.988,1.988A1.981,1.981,0,0,0,10.138,55.49Z" transform="translate(6.986 -40.038)" fill="#fff"/>
                                      <path id="Path_3680" data-name="Path 3680" d="M40.683,56.043C39.013,52.167,35.336,50,30.207,50H13.549A11.546,11.546,0,0,0,2,61.549V78.207c0,5.129,2.167,8.806,6.043,10.476a1.008,1.008,0,0,0,1.093-.219L40.464,57.136A.954.954,0,0,0,40.683,56.043ZM18.956,70.355a3.985,3.985,0,0,1-2.8,1.113,4.073,4.073,0,0,1-2.8-1.113c-2.028-1.908-4.254-4.95-3.4-8.567a6.19,6.19,0,0,1,6.2-4.751,6.188,6.188,0,0,1,6.2,4.771C23.19,65.406,20.964,68.447,18.956,70.355Z" transform="translate(0.911 -45.462)" fill="#fff"/>
                                      <path id="Path_3681" data-name="Path 3681" d="M31.293,73.726a.907.907,0,0,1-.159,1.451,12.916,12.916,0,0,1-6.361,1.471H8.115A.618.618,0,0,1,7.7,75.575L19.7,63.568a.984.984,0,0,1,1.411,0Z" transform="translate(6.345 -32.353)" fill="#fff"/>
                                      <path id="Path_3682" data-name="Path 3682" d="M28.658,56.115V72.773a12.862,12.862,0,0,1-1.471,6.361.918.918,0,0,1-1.451.159L15.558,69.115a.984.984,0,0,1,0-1.411L27.565,55.7A.63.63,0,0,1,28.658,56.115Z" transform="translate(14.01 -40.028)" fill="#fff"/>
                                    </g>
                                  </g>
                                </svg>
                            </div>
                            <h5><?php _e("Location","radix") ?></h5>
                            <a href="https://www.google.com.ph/maps/dir/<?php if(is_rtl()) { echo $mapaddress; } else { echo $mapaddress_en; } ?>" target="_blank">
                                <p><?php if(is_rtl()) { echo $address; } else { echo $address_en; } ?></p>
                            </a>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <svg id="Group_73549" data-name="Group 73549" xmlns="http://www.w3.org/2000/svg" width="48.927" height="41.588" viewBox="0 0 48.927 41.588">
                                  <path id="Path_3685" data-name="Path 3685" d="M38.695,51.5H14.232C6.893,51.5,2,55.17,2,63.732V80.856c0,8.562,4.893,12.232,12.232,12.232H38.695c7.339,0,12.232-3.669,12.232-12.232V63.732C50.927,55.17,46.034,51.5,38.695,51.5Zm1.15,14.9-7.657,6.116a9.444,9.444,0,0,1-11.449,0L13.082,66.4a1.882,1.882,0,0,1-.294-2.593,1.829,1.829,0,0,1,2.569-.294l7.657,6.116a5.837,5.837,0,0,0,6.874,0l7.657-6.116a1.805,1.805,0,0,1,2.569.294A1.856,1.856,0,0,1,39.845,66.4Z" transform="translate(-2 -51.5)" fill="#fff"/>
                                </svg>
                            </div>
                            <h5><?php _e("E-mail","radix"); ?></h5>
                            <a href="mailto:<?php echo $email; ?>"><p><?php echo $email; ?></p></a>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
    <div class="bottom">
        <div class="row no-gutters">
            <div class="col-12 col-lg-6 col-sm-12 col-xl-6">
                <div class="map">
                    <iframe
                      width="100%"
                      height="664"
                      frameborder="0" style="border:0"
                      src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCqgALOl7sGfdtpkctZ7a11GI638beGyqc&q=<?php if(is_rtl()) { echo $mapaddress; } else { echo $mapaddress_en; } ?>" allowfullscreen>
                    </iframe>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-sm-12 col-xl-6">
                <div class="form">
                  <?php if(is_rtl()) { echo do_shortcode('[gravityform id="2" title="true"]'); } else { echo do_shortcode('[gravityform id="37" title="true"]'); } ?>
                </div>
            </div>
        </div>
    </div>
</div>




<?php get_footer(''); ?>