<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content main and all content after
 *
 * @package radix
 * @since radix 1.0
 */
    global $phone;
    global $twitter;
    global $face;
    global $instagram;
    global $google;
    global $youtube;
    global $linkedin;
    global $snapchat;
    global $whats;
    global $pinterest;
    
?>


<footer id="footer-2">
    <div class="newsletter">
        <div class="text">
            <span><?php _e('SUBSCRIBE'); ?></span>
            <p><?php _e('TO OUR NEWSLETTER'); ?></p>
        </div>
        <div class="form">
            <?php echo do_shortcode('[email-subscribers namefield="NO" desc="" group="Public"] ') ?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php dynamic_sidebar('footer-widget-2'); ?>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container">
            <div class="row align-items-center justify-content-center ">
                <div class="footer__right">
                    <ul class="list-inline footer_social">
                        <?php if ( $face ) { ?>
                            <li class="list-inline-item facebook">
                                <a target="_blank" href="<?php echo $face ?>">
                                    <i class="flaticon-facebook-letter-logo"></i>
                                </a>
                            </li>
                         <?php } if( $twitter ) { ?> 
                            <li class="list-inline-item twitter">
                                <a target="_blank" href="<?php echo $twitter ?>">
                                    <i class="flaticon-twitter"></i>
                                </a>
                            </li>
                        <?php } if ( $whats ) { ?>
                            <li class="list-inline-item whatsapp">
                                <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo $whats ?>">
                                    <i class="flaticon-whatsapp"></i>
                                </a>
                            </li>
                        <?php } if ( $snapchat ) { ?>
                            <li class="list-inline-item snapchat">
                                <a target="_blank" href="<?php echo $snapchat ?>">
                                    <i class="flaticon-snapchat-1"></i>
                                </a>
                            </li>
                        <?php } if ( $youtube ) { ?>
                            <li class="list-inline-item youtube">
                                <a target="_blank" href="<?php echo $youtube ?>">
                                    <i class="flaticon-video-play-button"></i>
                                </a>
                            </li>
                        <?php } if ( $google ) { ?>
                            <li class="list-inline-item google">
                                <a target="_blank" href="<?php echo $google ?>">
                                    <i class="flaticon-google-plus"></i>
                                </a>
                            </li>
                        <?php } if( $instagram ) { ?>
                            <li class="list-inline-item instagram">
                                <a target="_blank" href="<?php echo $instagram ?>">
                                    <i class="flaticon-instagram-social-network-logo-of-photo-camera"></i>
                                </a>
                            </li>
                        <?php } if ( $pinterest ) { ?>
                            <li class="list-inline-item pinterest">
                                <a target="_blank" href="<?php echo $pinterest ?>">
                                   <i class="fab fa-pinterest-p"></i>
                                </a>
                            </li>
                        <?php } if ( $linkedin ) { ?>
                            <li class="list-inline-item linkedin">
                                <a target="_blank" href="<?php echo $linkedin ?>">
                                    <i class="flaticon-linkedin-logo-1"></i>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <p><?php _e('@  ESSCO Organization'); ?></p>
                    <p class="rights">
                    <?php _e('All rights reserved'); ?></p>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<script src="https://use.fontawesome.com/3504cbc9b2.js"></script>

<script>


	jQuery(window).load(function() {
                // will first fade out the loading animation
                // will fade out the whole DIV that covers the website.
            jQuery(".loading-screen").delay(2000).fadeOut("slow");
        })
        
$ = jQuery;


$(".navbar-nav > li a").attr("title","")
</script>


<script src="<?php echo get_template_directory_uri(); ?>/includes/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/demo6.js"></script>

<?php if(is_rtl()) { ?>
<script>
    jQuery('.newsletter .es-field-wrap input').attr('placeholder', 'البريد الالكتروني');
    jQuery('.newsletter input[type=submit]').val('اشترك');
   
</script>
<?php } else { ?>
<script>
    jQuery('.newsletter .es-field-wrap input').attr('placeholder', 'Enter Your email here');
    jQuery('.newsletter input[type=submit]').val('SUBSCRIBE');
   
</script>

<?php } ?>

<script>
$=jQuery;



    $(document).ready(function () {
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
       isClosed = false;

    function buttonSwitch() {

        if (isClosed === true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    trigger.click(function () {
        buttonSwitch();
    });
    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });
    
     $('.cl').click(function () {
         overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
            $('#wrapper').toggleClass('toggled');
    });

    $(".swiper-container").hover(function(){
        this.swiper.stopAutoplay();
        }, function(){
            this.swiper.startAutoplay();
        });
    });
    
    //Custom JS
$(document).ready(function(){
	$('a.target-burger').click(function(e){
		$('.viewport, nav.main-nav, a.target-burger').toggleClass('toggled');
		e.preventDefault();
	});
	$('.close-nav').click(function(e){
		$('.viewport, nav.main-nav, a.target-burger').toggleClass('toggled');
		e.preventDefault();
	});
	
});

// if($(window).width() > 767){
//     $('.col-xl-3 .search-wrap').remove();
// }else{
//     $('.col-xl-9 .search-wrap').remove();
// }

jQuery('.btn-search').click(function(e){
    let address = jQuery('#branch_city').val();
    console.log(address);
    window.open('https://www.google.com/maps/place/' + address, '_blank');
})


 $(".swiper-container").hover(function(){
        this.swiper.stopAutoplay();
        }, function(){
            this.swiper.startAutoplay();
    });
    
    $(window).load(function() {
        jQuery(".loading_screen").fadeOut(1500, function () {
    
              // Show The Scroll
    
             // jQuery("body").css("overflow", "auto");
    
            // jQuery(this).parent().fadeOut(1500, function () {
            
            //   jQuery(this).remove();
            // });
        });
    });
// $(document).ready(function(){
//     $("select#branch_city").change(function(){
//         var selectedCountry = $(this).children("option:selected").val();
//         alert("You have selected the country - " + selectedCountry);
//     });
// });


</script>



</body>
</html>