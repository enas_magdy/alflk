<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content main and all content after
 *
 * @package radix
 * @since radix 1.0
 */
        get_template_part( 'templates/content', 'contactInfo');

?>


    <footer>
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <?php dynamic_sidebar('footer-widget-1'); ?>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-8 col-12">
                        <div class="footer__right">
                            <p class="rights"><?php _e("All rights reserved to qmt Alfalk","radix"); ?></p>
                            <p><?php _e("The site works on handheld devices with high accuracy","radix"); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-4 col-12">
                        <div class="d-flex align-items-end flex-column">
                            <div class="rowaad">
                                <span>   
                                    <a href="https://rh.net.sa/" target="_blank" title="<?php _e("Web Design","radix") ?>">
                                        <?php _e("Design","radix") ?>
                                    </a>
                                    <a href="https://rh.net.sa/" target="_blank" title="<?php _e("Design","radix") ?>">
                                        <?php _e("And Development", "radix"); ?>
                                    </a>
                                </span>
                                <a href="https://www.rh.net.sa/" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="تصميم مواقع الكترونية">
                                    <svg id="Group_73576" data-name="Group 73576" xmlns="http://www.w3.org/2000/svg" width="78.644" height="55.065" viewBox="0 0 78.644 55.065">
                                      <path id="Path_34" data-name="Path 34" d="M644.36,739.189l8.347-5.342V693.78l-8.347,8.347Z" transform="translate(-615.799 -693.78)" fill="#008135"/>
                                      <path id="Path_35" data-name="Path 35" d="M469.94,837.785l8.347,8.347h11.922l-4.848,4.848-.048-.048H474.7l-1.516-1.516-.4.337,9.526,9.526H493.66V848.795l-10.7-11.01Z" transform="translate(-469.94 -814.204)" fill="#008135"/>
                                      <path id="Path_36" data-name="Path 36" d="M877.4,783.688V811.48l-4.276,4.276,1.294,1.294.476-.476,0,0,10.851-10.851V775.34Z" transform="translate(-807.1 -761.985)" fill="#008135"/>
                                      <path id="Path_37" data-name="Path 37" d="M725.574,777.542v31.526h.891l12.891-9.913-.2.2V813.8l-3.8,3.8.927.927.367.367L747.5,808.039V784.026l-21.035-6.26Zm.891,23.179V786.807l12.688,5.566Z" transform="translate(-683.714 -763.826)" fill="#008135"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>    
                </div>
                
            </div>
        </div>
    </footer>
</div>

<?php wp_footer(); ?>

<script src="https://use.fontawesome.com/3504cbc9b2.js"></script>

<script>


	jQuery(window).load(function() {
                // will first fade out the loading animation
                // will fade out the whole DIV that covers the website.
            jQuery(".loading-screen").delay(2000).fadeOut("slow");
        })
        
$ = jQuery;


$(".navbar-nav > li a").attr("title","")
</script>


<script src="<?php echo get_template_directory_uri(); ?>/includes/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/TweenMax.min.js"></script>

<script>
$=jQuery;

    $(document).ready(function () {
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
       isClosed = false;

    function buttonSwitch() {

        if (isClosed === true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    trigger.click(function () {
        buttonSwitch();
    });
    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });
    
     $('.cl').click(function () {
         overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
            $('#wrapper').toggleClass('toggled');
    });

    // $(".swiper-container").hover(function(){
    //     this.swiper.stopAutoplay();
    //     }, function(){
    //         this.swiper.startAutoplay();
    //     });
    });
    
    //Custom JS
$(document).ready(function(){
	$('a.target-burger').click(function(e){
		$('.viewport, nav.main-nav, a.target-burger').toggleClass('toggled');
		e.preventDefault();
	});
	$('.close-nav').click(function(e){
		$('.viewport, nav.main-nav, a.target-burger').toggleClass('toggled');
		e.preventDefault();
	});
	
});


$(document).ready(function(){
	$('#hamburger1').click(function(e){
		$('body').toggleClass('open__menu');
	});
	
});


    
    $(window).load(function() {
        jQuery(".loading_screen").fadeOut(1500, function () {
    
              // Show The Scroll
    
             // jQuery("body").css("overflow", "auto");
    
            // jQuery(this).parent().fadeOut(1500, function () {
            
            //   jQuery(this).remove();
            // });
        });
    });


</script>

<script>
$(function(){
    $(window).scroll(function() {              
        var scroll = ($(document).scrollTop() + $(window).height()) / $(document).height();

        
        if ($(window).scrollTop() >= 700) {
        $('.main-wrap').addClass('fixed-header');
    }
    else {
        $('.main-wrap').removeClass('fixed-header');
    }
        
    });
    
   
})

</script>
</body>
</html>