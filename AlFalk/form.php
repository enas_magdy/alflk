<?php
/**
 * Template Name: form-page
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */

get_header(); ?>


<div id="sub-header" itemtype="http://schema.org/WPHeader" itemscope="itemscope" role="banner">
    <div id="breadcrumbs">
        <?php if( get_field('breadcrumb_image') ){ ?>
				<img src="<?php the_field('breadcrumb_image'); ?>" />
			<?php } else { ?>
			
			<?php } ?>
        <div class="text">
    	    <h1><?php _e('Submit the form'); ?></h1>
            <?php if( get_field('breadcrumb_text') ){ ?>
    	        <p><?php echo get_field('breadcrumb_text'); ?></p>
    	    <?php } ?>    	
    	</div>
    </div>
</div>



<div class="form-page">
    <div class="form">
        <div class="container">
            <div class="row main-">
                <div class="col-md-6 col-sm-5 col-xs-12">
                    <div class="images">
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/images/form1_03.png" />
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/images/form2_03.png" />
                        <img src="<?php echo get_template_directory_uri(); ?>/includes/images/form2_07.png" />
                    </div>
                </div>
                <div class="col-md-6 col-sm-7 col-xs-12">
                    <div class="form-slider owl-carousel owl-theme">
                        <?php $i = 0; foreach ($point as $point_) { ?>
                            <?php $pointe = $point[$i]; ?>
                            <?php $pointe_content = $point_content[$i]; ?>
                            <?php $pointeen = $pointen[$i]; ?>
                            <?php $pointeen_content = $pointen_content[$i]; ?>
                            <div class="item">
                                <?php if(is_rtl()) { ?>
                                    <h2><?php echo $pointe; ?></h2>
                                    <p><?php echo $pointe_content; ?></p>
                                <?php } else { ?>
                                    <h2><?php echo $pointeen; ?></h2>
                                    <p><?php echo $pointeen_content; ?></p>
                                <?php } ?>                        
                            </div>
                        <?php  $i++; } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <?php echo the_content(); ?>
    </div>
    
    <div class="newsletter" style="background:url('<?php echo esc_url($newsletter_back['url']); ?>')">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text">
                        <h1><?php _e('NewsLetter'); ?></h1>
                        <p><?php _e('Subscribe to receive all new offers and news'); ?></p>
                    </div>
                </div>
                <div class="col-md-7">
                    <?php echo do_shortcode('[email-subscribers namefield="NO" desc="" group="Public"] ') ?>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="text">
                        <h5><?php _e('Contact Us'); ?></h5>
                        <p><?php _e('You can now communicate with us through communication methods to learn more'); ?></p>
                    </div>
                </div> 
                <div class="col-md-8 col-sm-8">
                    <div class="info">
                        <ul class="list-inline">
                            <?php if(!empty($email)) { ?>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="p">
                                        <p><?php _e('E-mail'); ?></p>
                                        <span><?php echo $email; ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if(!empty($phone)) { ?>
                                <li>
                                    <i class="flaticon-phone-receiver"></i>
                                    <div class="p">
                                        <p><?php _e('Phone Number'); ?></p>
                                        <span><?php echo $phone; ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if(is_rtl()) { ?>
                                <?php if(!empty($contact)) { ?>
                                    <li>
                                        <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
                                        <div class="p">
                                            <p><?php _e('Address'); ?></p>
                                            <span><?php echo $contact; ?></span>
                                        </div>
                                    </li>
                                <?php } ?>
                            <?php }  else { ?>
                                <?php if(!empty($contacten)) { ?>
                                    <li>
                                        <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
                                        <div class="p">
                                            <p><?php _e('Address'); ?></p>
                                            <span><?php echo $contacten; ?></span>
                                        </div>
                                    </li>
                                <?php } 
                            } ?>
                        </ul>
                    </div>
                </div> 
            </div>        
        </div>
    </div>
</div>

<? get_footer(); ?>