<?php

/**
 * radix - setup functions and definitions
 * 
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 * 
 * @package radix
 * @author Abukwaik http://www.radixtheme.com
 * @copyright Copyright (c) 2015, radix
 * @license http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @since radix 1.0
 *
 */

define('radix_THEME_NAME', 'radix');
define('radix_THEME_SLUG', 'radix');
define('radix_THEME_VERSION', '1.1.2');
define('radix_THEME_OPTIONS', 'ro_settings');
define('radix_THEME_DIR', get_template_directory());
define('radix_THEME_URI', get_template_directory_uri());
define('radix_JS_URI',  radix_THEME_URI . '/includes/js');
define('radix_CSS_URI', radix_THEME_URI . '/includes/css');
define('radix_IMG_DIR', radix_THEME_DIR . '/images');
define('radix_IMG_URI', radix_THEME_URI . '/images');

if ( ! isset( $content_width )) {
  $content_width = 669; 
}

// Set the content width for full width pages with no sidebar.
function radix_content_width() {
  if ( is_page_template( 'page-fullwidth.php' ) || is_page_template( 'front-page.php' ) ) {
    global $content_width;
    $content_width = 1170;
  }
}
add_action( 'template_redirect', 'radix_content_width' );


/**
    Theme setup
**/

if ( !function_exists( 'radix_setup' ) ) {

  function radix_setup() {
    add_theme_support( 'automatic-feed-links');
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-formats', array( 'audio', 'gallery', 'image', 'video' ));
    add_theme_support( 'post-thumbnails');
    load_theme_textdomain( 'radix', radix_THEME_DIR . '/languages' );
    add_editor_style( radix_CSS_URI . '/editor-style.css' );

    // Register Menus
    register_nav_menus(
      array(
        'primary'     => __( 'Primary', 'radix' ),
        'footer-menu' => __( 'Footer Menu', 'radix' ),
        'top-menu' => __( 'top Menu', 'radix' ),
        'side-menu' => __( 'side Menu', 'radix' ),
        ) 
      );

    // allows users to set a custom background.
    add_theme_support( 'custom-background', apply_filters( 'radix_custom_background_args', array(
      'default-color' => 'f5f5f5',
      )));

    //add image sizes
    add_image_size('radix-featured_image', 669, 272, true);
    add_image_size('radix-small-thumb',  50, 50, true);
    add_image_size('pro-image', 600, 450, true);
    add_image_size('news-image', 500, 420, true);
    add_image_size('radix-full-size', 9999, 9999, false);
  }
}

add_action( 'after_setup_theme', 'radix_setup' );

/**
  Title backwards compatibility for old WP versions
*/
if ( ! function_exists( '_wp_render_title_tag' ) ) {
  function theme_slug_render_title() { ?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php }
  add_action( 'wp_head', 'theme_slug_render_title' );
}

/**
    Getting post thumbnail url
 */
function radix_get_thumb_url($pots_ID){
  return wp_get_attachment_url( get_post_thumbnail_id( $pots_ID ) );
}


/**
    Load Required Files
**/
// Load Redux framework admin panel
if ( !class_exists( 'ReduxFramework' ) ) {
  require_once radix_THEME_DIR . '/admin/ReduxCore/framework.php';
  require_once radix_THEME_DIR . '/admin/ReduxCore/redux-config.php';
}

// Required Redux framework functions
require_once radix_THEME_DIR . '/admin/theme-options.php';

// radix Styles and Scripts 
require_once radix_THEME_DIR . '/admin/core/scripts.php';

// radix Sidebars, widgets and menus
require_once radix_THEME_DIR . '/admin/core/register.php';

// Custom functions & snippets
require_once radix_THEME_DIR . '/admin/core/clean.php';
require_once radix_THEME_DIR . '/admin/core/snippets.php';
require_once radix_THEME_DIR . '/admin/core/jetpack.php';

// Bootstrap Style Breadcrumbs
require_once radix_THEME_DIR . '/includes/breadcrumbs.php';

// Bootstrap nav walker
require_once radix_THEME_DIR . '/includes/bootstrap-walker.php';

// Bootstrap Pagination
require_once radix_THEME_DIR . '/includes/bootstrap-pagination.php';

// Custom template tags
require_once radix_THEME_DIR . '/includes/template-tags.php';

// Implement Custom Header features.
require_once radix_THEME_DIR . '/includes/custom-header.php';

// Social share
require_once radix_THEME_DIR . '/includes/social-share.php';

require_once radix_THEME_DIR . '/includes/global-variables.php';

require_once radix_THEME_DIR . '/includes/post-types/projects.php';

require_once radix_THEME_DIR . '/includes/post-types/products.php';
require_once radix_THEME_DIR . '/includes/post-types/products-category.php';

require_once radix_THEME_DIR . '/includes/post-types/partners.php';

require_once radix_THEME_DIR . '/includes/post-types/governance.php';

require_once radix_THEME_DIR . '/includes/post-types/bank-accounts.php';



add_action('admin_head', 'my_custom_fonts'); 
function my_custom_fonts() { echo '<style>
@import url("https://fonts.googleapis.com/css?family=Cairo");
body, td, textarea, input, select { font-family: "Cairo" !important;} </style>'; }


add_action('show_admin_bar', '__return_false');

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
    
    // loop
    foreach( $items as &$item ) {
        
        // vars
        $icon = get_field('menu_item_icon', $item);
        
        
        // append icon
        if( $icon ) {
            
            $item->title .= ' <img src="'.$icon.'"/>';
            
        }
        
    }
    
    
    // return
    return $items;
    
}

