<?php
/**
 * Template Name: governance
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
get_header(); 
?>

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / <?php the_title(); ?></h3>
        </div>
    </div>
</div>


<div class="governance-page">
    <div class="container">
        <div class="main_title">الحوكمة</div>
        <div class="row">
            <?php
                $terms = get_terms('governance_category',array(
                    'hide_empty' => false,
                ) );
                foreach($terms as $term) {
            ?>
            <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4 col-6">
                <div class="item">
                    <a href="<?php echo get_term_link($term);?>">
        				<div class="image text-center">
                            <?php $icon = get_field('category_icon', $term->taxonomy.'_'.$term->term_id); 
                                $thumb = wp_get_attachment_image_src($icon, "radix-full-size"); 
                            ?>
                            <img src="<?php echo $thumb[0]; ?>" />                            
                            <div class="name text-center">
                                <h4><?php echo $term->name ?></h4>
                            </div>
                        </div>
            		</a>
        		</div>
        	</div>
        	<?php } ?>
        </div>
    </div>
</div>



<?php get_footer(''); ?>