<?php
/**
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package radix
 * @since radix 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js smoothScroll">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" type="image/x-icon" href="<?php echo ro_get_option("favicon")['url']; ?>">
<?php wp_head(); ?>
	
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

</head>
<body  <?php body_class(); ?>  itemscope="itemscope" itemtype="http://schema.org/WebPage">

<?php 
    global $twitter;
    global $face;
    global $instagram;
    global $google;
    global $youtube;
    global $tripadvisor;
    global $linkedin;
    global $snapchat;
    global $whats;
    global $address;
    global $address_ar;
    global $phone;
    global $worktimes;
    global $worktimes_ar;
?>

<!--<div class="loading_screen">-->
<!--<span class="loader"></span>-->
<!--</div>-->
<header class="main-wrap">
    <div class="container">
        <div class="row align-items-center justify-content-between no-gutters">
            <div class="col-lg-2 col-xl-2 col-md-12 col-sm-12 col-12">
                <div class="d-flex align-items-center justify-content-between no-gutters">
                    <div class="header__logo">
                        <?php $logo = ro_get_option('logo'); ?>
                           <?php if(!empty($logo['url'])) : ?>
                               <a itemprop="url" href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" >
                                   <img itemprop="logo" width="<?php echo esc_attr( 280 ); ?>" height="<?php echo esc_attr( 67 ); ?>" src="<?php echo esc_url( $logo['url'] ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
                               </a>                 
                           <?php else: ?>
                               <h1 class="site-title" itemprop="headline">
                                   <a href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                               </h1>
                       <?php endif; ?>
                    </div>
                    <div class="d-lg-none d-block content">
                        <nav>
                            <button  class="btn btn-menu" id="btn-menu">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.205" height="20.373" viewBox="0 0 27.205 20.373">
                                  <path id="bars-solid_1_" data-name="bars-solid (1)" d="M141,66.91A2.907,2.907,0,0,1,143.91,64h21.384a2.91,2.91,0,0,1,0,5.821H143.91A2.907,2.907,0,0,1,141,66.91Zm0,14.552a2.907,2.907,0,0,1,2.91-2.91h21.384a2.91,2.91,0,0,1,0,5.821H143.91A2.907,2.907,0,0,1,141,81.463Z" transform="translate(-141 -64)" fill="#f1592a"/>
                                </svg>
                            </button>
                            <div class="request">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#QuotationModalHeader">
                                    <?php _e("Request for Quotation","radix") ?>
                                </button>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-xl-8 col-md-none col-sm-none col-12 d-lg-block d-none">
                <div class="d-flex align-items-center justify-content-center no-gutters">
                    <div class="home">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20.343" height="20" viewBox="0 0 20.343 20">
                          <path id="Path_3693" data-name="Path 3693" d="M20.83,8.01,14.28,2.77a3.765,3.765,0,0,0-4.55-.01L3.18,8.01a3.917,3.917,0,0,0-1.31,3.43l1.26,7.54A3.716,3.716,0,0,0,6.7,22H17.3a3.773,3.773,0,0,0,3.58-3.03l1.26-7.54A4.018,4.018,0,0,0,20.83,8.01ZM12.75,18a.75.75,0,0,1-1.5,0V15a.75.75,0,0,1,1.5,0Z" transform="translate(-1.83 -2)" fill="#008135"/>
                        </svg>
                    </div>
                    <?php wp_nav_menu(); ?>
                    <div class="search">
                        <form class="search__form" action="">
                    		<input class="search__input" name="s" type="search" placeholder="<?php _e("Search Here....","radix") ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
                    		<button type="submit" class="btn btn-default">
                    		    <svg id="search-normal" xmlns="http://www.w3.org/2000/svg" width="32.533" height="32.533" viewBox="0 0 32.533 32.533">
                                  <path id="Vector" d="M25.755,12.878A12.878,12.878,0,1,1,12.878,0,12.878,12.878,0,0,1,25.755,12.878Z" transform="translate(2.711 2.711)" fill="none" stroke="#008135" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                  <path id="Vector-2" data-name="Vector" d="M1.88,1.88-3-3" transform="translate(27.942 27.942)" fill="none" stroke="#008135" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                  <path id="Vector-3" data-name="Vector" d="M0,0H32.533V32.533H0Z" fill="none" opacity="0"/>
                                </svg>
                    		</button>
                    	</form>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-2 col-xl-2 col-md-none col-sm-12 col-12 d-lg-block d-none">
                <div class="header-bottom d-flex align-items-center justify-content-end">
                    <?php radix_top_nav(); ?>
                    <div class="request">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#QuotationModalHeader">
                            <?php _e("Request for Quotation","radix") ?>
                        </button>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
    
    <div class="menu__wraper">
        <a itemprop="url" href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" >
           <img itemprop="logo" width="<?php echo esc_attr( 80 ); ?>" height="<?php echo esc_attr( 80 ); ?>" src="<?php echo esc_url( $logo['url'] ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
        </a>
		 <?php wp_nav_menu(); ?>
		 <?php radix_top_nav(); ?>
		 <form class="search__form" action="">
    		<input class="search__input" name="s" type="search" placeholder="<?php _e("Search Here....","radix") ?>" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
    	    <button type="submit" class="btn btn-default">
    		    <svg id="search-normal" xmlns="http://www.w3.org/2000/svg" width="32.533" height="32.533" viewBox="0 0 32.533 32.533">
                  <path id="Vector" d="M25.755,12.878A12.878,12.878,0,1,1,12.878,0,12.878,12.878,0,0,1,25.755,12.878Z" transform="translate(2.711 2.711)" fill="none" stroke="#008135" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                  <path id="Vector-2" data-name="Vector" d="M1.88,1.88-3-3" transform="translate(27.942 27.942)" fill="none" stroke="#008135" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                  <path id="Vector-3" data-name="Vector" d="M0,0H32.533V32.533H0Z" fill="none" opacity="0"/>
                </svg>
    		</button>
    	</form>

	</div>
	<button id="btn-menu-close" class="btn btn--hidden btn--menu-close" aria-label="Close menu form">
	    <svg class="icon icon--cross"><path fill="#000" d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>
	</button>

</header>


<div class="modal fade" id="QuotationModalHeader" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php _e("Request for Quotation","radix") ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php if(is_rtl()) { echo do_shortcode('[gravityform id="38" title="true"]'); } else { echo do_shortcode('[gravityform id="39" title="true"]'); } ?>
      </div>
    </div>
  </div>
</div>



<!--<div class="container-fluid">-->


