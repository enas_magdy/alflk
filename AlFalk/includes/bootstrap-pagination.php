<?php 

/**
 * Bootstrap Style Pagination
 *
 * @package radix
 * @since radix 1.0
 */

/**
    Bootstrap pagination
**/

if( ! function_exists('radix_pagination')) {
	function radix_pagination( $args = array() ) {
		$defaults = array(
			'range'     	  => 4,
			'custom_query'    => FALSE,
			'previous_string' => '<span><svg xmlns="http://www.w3.org/2000/svg" width="23.93" height="23.93" viewBox="0 0 23.93 23.93"><g id="arrow-down" transform="translate(0)"><path id="Vector" d="M15.794,0l-6.5,6.5A1.98,1.98,0,0,1,6.5,6.5L0,0" transform="translate(4.068 8.924)" fill="none" stroke="#707070" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path id="Vector-2" data-name="Vector" d="M0,0H23.93V23.93H0Z" transform="translate(23.93 23.93) rotate(180)" fill="none" opacity="0"/></g></svg></span>', 'radix',
			'next_string' 	  => '<span><svg xmlns="http://www.w3.org/2000/svg" width="23.93" height="23.93" viewBox="0 0 23.93 23.93"><g id="vuesax_linear_arrow-up" data-name="vuesax/linear/arrow-up" transform="translate(23.93) rotate(90)"> <g id="arrow-up" transform="translate(0)"><path id="Vector" d="M15.794,7.077l-6.5-6.5A1.98,1.98,0,0,0,6.5.576L0,7.077" transform="translate(4.068 7.929)" fill="none" stroke="#707070" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path id="Vector-2" data-name="Vector" d="M0,0H23.93V23.93H0Z" transform="translate(23.93 23.93) rotate(180)" fill="none" opacity="0"/></g></g></svg></span>', 'radix',
			'before_output'   => '<div class="post-nav"><ul class="pager">',
			'after_output' 	  => '</ul></div>'
			);
		$args = wp_parse_args(
			$args,
			apply_filters( 'wp_bootstrap_pagination_defaults', $defaults ));
		$args['range'] = (int) $args['range'] - 1;
		if ( !$args['custom_query'] )
			$args['custom_query'] = @$GLOBALS['wp_query'];
		$count = (int) $args['custom_query']->max_num_pages;
		$page = intval( get_query_var( 'paged' ) );
		$ceil = ceil( $args['range'] / 2 );
		if ( $count <= 1 )
			return FALSE;
		if ( !$page )
			$page = 1;
		if ( $count > $args['range'] ) {
			if ( $page <= $args['range'] ) {
				$min = 1;
				$max = $args['range'] + 1;
			} elseif ( $page >= ($count - $ceil) ) {
				$min = $count - $args['range'];
				$max = $count;
			} elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
				$min = $page - $ceil;
				$max = $page + $ceil;
			}
		} else {
			$min = 1;
			$max = $count;
		}
		$echo = '';
		$previous = intval($page) - 1;
		$previous = esc_attr( get_pagenum_link($previous) );
		$firstpage = esc_attr( get_pagenum_link(1) );
			if ( $previous && (1 != $page) )
			$echo .= '<li><a class="arr prev" href="' . $previous . '" title="' . __( 'Previous', 'radix') . '">' . $args['previous_string'] . '</a></li>';
		if ( $firstpage && (1 != $page) )
			$echo .= '<li class="previous"><a href="' . $firstpage . '">' . __( ' ... ', 'radix' ) . '</a></li>';
		if ( !empty($min) && !empty($max) ) {
			for( $i = $min; $i <= $max; $i++ ) {
				if ($page == $i) {
					$echo .= '<li class="active"><span class="active">' . (int)$i . '</span></li>';
				} else {
					$echo .= sprintf( '<li><a href="%s">%2d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
				}
			}
		}
		$next = intval($page) + 1;
		$next = esc_attr( get_pagenum_link($next) );
		
		$lastpage = esc_attr( get_pagenum_link($count) );
		if ( $lastpage ) {
			$echo .= '<li class="next"><a href="' . $lastpage . '">' . __( ' ... ', 'radix' ) . '</a></li>';
		}
		if ($next && ($count != $page) )
			$echo .= '<li><a class="arr" href="' . $next . '" title="' . __( 'Next', 'radix') . '">' . $args['next_string'] . '</a></li>';
		if ( isset($echo) )
			echo $args['before_output'] . $echo . $args['after_output'];
	}
}
