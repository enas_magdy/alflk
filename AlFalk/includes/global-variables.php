<?php  

    $slider = ro_get_option('slider_section');
    $slider_id = ro_get_option('slider_id');
    $slider_id_en = ro_get_option('slider_id_en');

    $about_image = ro_get_option('about_img');
    $about_title = ro_get_option('about_title');
    $about_title_en = ro_get_option('about_title_en');
    $about_content = ro_get_option('about_content');
    $about_content_en = ro_get_option('about_content_en');
    $about_section = ro_get_option('about_section');

    $products_section = ro_get_option('products_section');
    
    $statistics_section = ro_get_option('statistics_section');
    $statistics_image = ro_get_option('statistics_image');
    $statistics_img = ro_get_option('statistics_img');
    $statistics_title = ro_get_option('statistics_title');
    $statistics_title_en = ro_get_option('statistics_title_en');
    $statistics_content = ro_get_option('statistics_content');
    $statistics_content_en = ro_get_option('statistics_content_en');
    
    $projects_section = ro_get_option('projects_section');

    $partners_section = ro_get_option('partners_section');
    
    $news_section = ro_get_option('news_section');
    $newsletter_section = ro_get_option('newsletter_section');
    
    $vision_section = ro_get_option('vision_section');
    $vision_img = ro_get_option('vision_img');
    $vision_content = ro_get_option('vision_content');
    $vision_content_en = ro_get_option('vision_content_en');

    

    $aboutus_text = ro_get_option('about_content');
    $our_vision = ro_get_option('vision');
    $message= ro_get_option('message');
    $about_page_img= ro_get_option('about_page_img');
   


    $twitter = ro_get_option('twitter');
    $face = ro_get_option('facebook');
    $youtube = ro_get_option('youtube');
    $instagram = ro_get_option('instagram');
    $linkedin = ro_get_option('linkedin');
    $snapchat = ro_get_option('snapchat');

    
    $feature_name = ro_get_option('feature_name');
    $feature_name_en = ro_get_option('feature_name_en');
    $feature_img = ro_get_option('feature_img');
    $feature_desc = ro_get_option('feature_desc');
    $feature_desc_en = ro_get_option('feature_desc_en');

    $whats = ro_get_option('whats');
    $phone = ro_get_option('phone');
    $email = ro_get_option('email');
    $address = ro_get_option('address');
    $mapaddress = ro_get_option('mapaddress');
    $address_en = ro_get_option('address_en');
    $mapaddress_en = ro_get_option('mapaddress_en');
    


?>