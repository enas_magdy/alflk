/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2016, Codrops
 * http://www.codrops.com
 */

;(function(window) {

	'use strict';

	var mainContainer = document.querySelector('.main-wrap'),
		openCtrl = document.getElementById('btn-menu'),
		closeCtrl = document.getElementById('btn-menu-close'),
		menuContainer = document.querySelector('.menu__wraper');
// 		inputMenu = menuContainer.querySelector('.menu__input');

	function init() {
		initEvents();	
	}

	function initEvents() {
		openCtrl.addEventListener('click', openMenu);
		closeCtrl.addEventListener('click', closeMenu);
		document.addEventListener('keyup', function(ev) {
			// escape key.
			if( ev.keyCode == 27 ) {
				closeMenu();
			}
		});
	}

	function openMenu() {
		mainContainer.classList.add('main-wrap--overlay');
		closeCtrl.classList.remove('btn--hidden');
		menuContainer.classList.add('menu--open');
		setTimeout(function() {
// 			inputMenu.focus();
		}, 500);
	}

	function closeMenu() {
		mainContainer.classList.remove('main-wrap--overlay');
		closeCtrl.classList.add('btn--hidden');
		menuContainer.classList.remove('menu--open');
// 		inputMenu.blur();
// 		inputMenu.value = '';
	}

	init();

})(window);;