<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package radix
 * @since radix 1.0
 *
 */
    
get_header(); ?>

<?php 
    get_template_part( 'templates/content', 'slider');
    get_template_part( 'templates/content', 'about');
    get_template_part( 'templates/content', 'products');
    get_template_part( 'templates/content', 'statistics');
    get_template_part( 'templates/content', 'projects');
    get_template_part( 'templates/content', 'partners');
    get_template_part( 'templates/content', 'news');
    get_template_part( 'templates/content', 'vision');
    get_template_part( 'templates/content', 'newsletter');

?>

<?php get_footer(); ?>