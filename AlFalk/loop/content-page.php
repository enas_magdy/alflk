<?php
/**
 * @package radix
 * @since radix 1.0
 * 
 */
?>

<article id="post-<?php the_ID(); ?>" itemtype="http://schema.org/BlogPosting" itemscope="itemscope">

    <a href="<?php echo the_post_thumbnail_url('radix-full-size') ?>" data-fancybox="">
        <img src="<?php the_post_thumbnail_url('radix-full-size');  ?>" />  
    </a>
    <div class="content_">
        <div class="text wow fadeInUp">
        	<div class="entry-content clearfix">
        		<?php
        			/* translators: %s: Name of current post */
        			the_content( sprintf(
        				__( 'Continue reading %s', 'radix' ),
        				the_title( '<span class="screen-reader-text">', '</span>', false )
        				) );
        
        			wp_link_pages( array(
        				'before'      	 => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'radix' ) . '</span>',
        				'after'       	 => '</div>',
        				'link_before' 	 => '<span>',
        				'link_after'  	 => '</span>',
        				'current_before' => '',
        				'current_after'  => '',
        				'pagelink'    	 => '%',
        			) );
        		?>
        	</div>
        </div>
    </div>

</article><!-- #post-## -->
