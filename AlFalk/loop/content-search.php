<?php
/**
 * @package radix
 * @since radix 1.0
 *
 */
?>

<div class="col-md-4 col-sm-4 col-xl-4 col-lg-4 col-6">
    <div class="item">
	    <div class="image text-center">
            <img src="<?php the_post_thumbnail_url('image');  ?>" />
        </div>
        <div class="caption">
            <a href="<?php echo get_permalink();?>">
                <h3><?php echo the_title(); ?></h3>
                
            </a>
        </div>
	</div>
</div>
