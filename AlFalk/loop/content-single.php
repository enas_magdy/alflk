<?php
/**
 * @package radix
 * @since radix 1.0
 *
 */
 
?>

<?php if (  'projects'  == get_post_type() || 'post'  == get_post_type() ) { ?> 
<div class="project-single">
    <h3 class="main_title"><?php the_title(); ?></h3>
    <div class="row">
        <div class="col-12 col-lg-5 col-xl-5 col-md-12 col-sm-12">
            <div class="main-image">
                <a href="<?php echo the_post_thumbnail_url('radix-full-size') ?>" data-fancybox="group">
                    <img src="<?php the_post_thumbnail_url('radix-full-size');  ?>" />  
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-7 col-xl-7 col-md-12 col-sm-12">
            <div class="single__header">
                <?php if ( 'post'  == get_post_type() ) { ?>
                <h5 class="date"><?php echo date_i18n( get_option( 'date_format' ), strtotime( '11/15-1976' ) ); ?></h5>
                <?php } ?>
                <h3 class="entry-title"><?php the_title(); ?></h3>
                <p>
            		<?php
            			/* translators: %s: Name of current post */
            			the_content( sprintf(
            				__( 'Continue reading %s', 'radix' ),
            				the_title( '<span class="screen-reader-text">', '</span>', false )
            				) );
            
            			wp_link_pages( array(
            				'before'      	 => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'radix' ) . '</span>',
            				'after'       	 => '</div>',
            				'link_before' 	 => '<span>',
            				'link_after'  	 => '</span>',
            				'current_before' => '',
            				'current_after'  => '',
            				'pagelink'    	 => '%',
            			) );
            		?>
            	</p>
            </div>  
        </div>    
    </div>    
</div>
<?php } else if (  'products'  == get_post_type() ) { ?> 
<div class="product-single">
    <h3 class="main_title"><?php the_title(); ?></h3>
    <div class="row">
        <div class="col-12 col-lg-5 col-xl-5 col-md-12 col-sm-12">
            <div class="main-image">
                <a href="<?php echo the_post_thumbnail_url('radix-full-size') ?>" data-fancybox="group">
                    <img src="<?php the_post_thumbnail_url('radix-full-size');  ?>" />  
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-7 col-xl-7 col-md-12 col-sm-12">
            <div class="single__header">
                <h3 class="entry-title"> <?php the_title(); ?></h3>
                <p>
            		<?php
            			/* translators: %s: Name of current post */
            			the_content( sprintf(
            				__( 'Continue reading %s', 'radix' ),
            				the_title( '<span class="screen-reader-text">', '</span>', false )
            				) );
            
            			wp_link_pages( array(
            				'before'      	 => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'radix' ) . '</span>',
            				'after'       	 => '</div>',
            				'link_before' 	 => '<span>',
            				'link_after'  	 => '</span>',
            				'current_before' => '',
            				'current_after'  => '',
            				'pagelink'    	 => '%',
            			) );
            		?>
            	</p>
            	<div class="request">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#QuotationModalHeader">
                        <?php _e("Request for Quotation","radix") ?>
                    </button>
                </div>
            </div>  
        </div>
    </div>    
</div>
<?php } ?>
