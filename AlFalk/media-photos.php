<?php
/**
 * Template Name: photos
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
get_header(); 
?>

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / 
            <a href="<?php echo get_post_type_archive_link("media"); ?>">المركز الاعلامي</a> /
            <?php the_title(); ?></h3>
        </div>
    </div>
</div>

<div class="archive__media media">
    <div class="container">
        <h3 class="main_title"><?php the_title(); ?></h3>
    </div>    
    <div class="videos">
        <div class="container">
            <div class="row">
                <?php
                   $query = new WP_Query( array(
                           'post_type'     => 'media', //your post type
                           'posts_per_page' => -1,
                           'order' => 'ASC',
                           'meta_key'      => 'media_type',
                           'meta_value'    => 'صورة'
                       )
                   );
               
                   while ($query->have_posts()) {
                       $query->the_post();
                       //whatever code you want
                ?>    
                <div class="col-xl-6 col-lg-6 col-sm-6 col-md-6 col-12">
                    <div class="item d-flex align-items-center">
        			    <div class="image text-center">
        			        <a href="<?php echo the_field("image"); ?>" data-fancybox="group">
                                <img src="<?php the_post_thumbnail_url('image');  ?>" />
                            </a>
                        </div>
                        <div class="caption">
                            <h3><?php echo mb_strimwidth(strip_tags(get_the_title()) , 0, 33, '','utf-8'); ?></h3>
                            <p><?php echo mb_strimwidth(strip_tags(get_the_content()) , 0, 80, '','utf-8'); ?></p>
                        </div>
            		</div>
            	</div>
            	<?php } ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(''); ?>