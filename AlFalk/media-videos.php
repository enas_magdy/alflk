<?php
/**
 * Template Name: videos
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
get_header(); 
?>

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / 
                        <a href="<?php echo get_post_type_archive_link("media"); ?>">المركز الاعلامي</a> /

            <?php the_title(); ?></h3>
        </div>
    </div>
</div>

<div class="archive__media media">
    <div class="container">
        <h3 class="main_title"><?php the_title(); ?></h3>
    </div>    
    <div class="videos">
        <div class="container">
            <div class="row">
                <?php
                   $query = new WP_Query( array(
                           'post_type'     => 'media', //your post type
                           'posts_per_page' => -1,
                           'order' => 'ASC',
                           'meta_key'      => 'media_type',
                            'meta_value'    => 'فيديو'
                       )
                   );
               
                   while ($query->have_posts()) {
                       $query->the_post();
                       //whatever code you want
                ?>    
                <div class="col-xl-6 col-lg-6 col-sm-6 col-md-6 col-12">
                    <div class="item d-flex align-items-center">
        			    <div class="image text-center">
        			        <a href="<?php echo the_field("video"); ?>" data-fancybox="group">
                                <img src="<?php the_post_thumbnail_url('image');  ?>" />
                                <?php if(get_field("media_type")['value'] === "فيديو" ){ ?>
                                    <div class="icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="49" height="49" viewBox="0 0 49 49">
                                          <g id="Group_72989" data-name="Group 72989" transform="translate(-1433 -8901)">
                                            <g id="Group_72990" data-name="Group 72990">
                                              <rect id="Rectangle_1491" data-name="Rectangle 1491" width="49" height="49" rx="24.5" transform="translate(1433 8901)" fill="#fff"/>
                                              <g id="Group_72983" data-name="Group 72983" transform="translate(1446 8914.991)">
                                                <path id="Path_66099" data-name="Path 66099" d="M18,6.834,9.82.837A4.287,4.287,0,0,0,3,4.293v12a4.282,4.282,0,0,0,6.82,3.455l8.178-6a4.287,4.287,0,0,0,0-6.912Z" transform="translate(0 0)"/>
                                              </g>
                                            </g>
                                          </g>
                                        </svg>
                                    </div>
                                <?php } ?>
                            </a>
                        </div>
                        <div class="caption">
                            <h3><?php echo mb_strimwidth(strip_tags(get_the_title()) , 0, 33, '','utf-8'); ?></h3>
                            <p><?php echo mb_strimwidth(strip_tags(get_the_content()) , 0, 80, '','utf-8'); ?></p>
                        </div>
            		</div>
            	</div>
            	<?php } ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer(''); ?>