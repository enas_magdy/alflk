<?php 

/**
 * Template Name: blog-page
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
 
 
get_header(); ?>

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
  <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
</svg>

            <?php echo the_title(); ?></h3>
        </div>
    </div>
</div>


<div class="news news-page blog-page">
    <div class="container">
        <h3 class="wow fadeInRight main_title" data-wow-delay="0.1s"><?php _e("News","radix") ?></h3>
        <div class="row no-gutters">
            <?php

               $query = new WP_Query( array(
                       'post_type'     => 'post', //your post type
                       'posts_per_page' => -1,
                        'paged' => $paged,
                   )
               );
               if ( have_posts() ) {
           
               while ($query->have_posts()) {
                   $query->the_post();
                   //whatever code you want
            ?>     
            <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4 col-12">
                <div class="item">
    			    <div class="image text-center">
                        <img src="<?php the_post_thumbnail_url('image');  ?>" />
                    </div>
                    <div class="caption">
                        <h3><?php echo the_title(); ?></h3>
                        <p><?php echo mb_strimwidth(strip_tags(get_the_content()) , 0, 80, '','utf-8'); ?></p>
                        <a class="btn btn-more" href="<?php echo get_permalink();?>">
                            <?php _e("More Details","radix"); ?>
                        </a>
                    </div>
        		</div>
    		</div>
    		<?php } } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>