<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package radix
 * @since radix 1.0
 */

get_header();  ?>     

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / <?php the_title(); ?></h3>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="main_title"><?php the_title(); ?></h3>
        	<?php while ( have_posts() ) : the_post(); ?>
        		<?php get_template_part( 'loop/content', 'page' ); ?>
        		<?php if (ro_get_option('comments_on_pages')) { ?>
        		<?php if ( comments_open() || '0' != get_comments_number() )
        		comments_template(); ?>
        		<?php } ?> 
        	<?php endwhile; // end of the loop. ?>
</div>
<?php get_footer(); ?>