<?php
/**
 * Template Name: plans-page
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */
get_header(); 

?>

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / <?php the_title(); ?></h3>
        </div>
    </div>
    <div class="image">
        <img src="<?php echo get_template_directory_uri(); ?>/includes/images/breadcrumb.png" />
    </div>
</div>


<div class="news my-5 py-4">
    <div class="container">
        
        <div class="row no-gutters">
        <?php
           $query = new WP_Query( array(
                   'post_type'     => 'plans', //your post type
                   'posts_per_page' => -1,
                   'order' => 'Des',
               )
           );
       
           while ($query->have_posts()) {
               $query->the_post();
               //whatever code you want
        ?>    
         <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4 col-12">
            <div class="item">
			    <div class="image text-center">
                    <img src="<?php the_post_thumbnail_url('image');  ?>" />
                </div>
                <div class="caption">
                    <h3><?php echo the_title(); ?></h3>
                    <p><?php echo mb_strimwidth(strip_tags(get_the_content()) , 0, 80, '','utf-8'); ?></p>
                    <a href="<?php echo get_permalink();?>">
                        <?php _e("More","radix"); ?>
                        <svg id="vuesax_linear_arrow-left" data-name="vuesax/linear/arrow-left" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                          <g id="arrow-left">
                            <path id="Vector" d="M6.07,13.64A1.5,1.5,0,0,1,5.009,13.2l-6.07-6.07A1.5,1.5,0,0,1-1.5,6.07a1.5,1.5,0,0,1,.439-1.061l6.07-6.07a1.5,1.5,0,0,1,2.121,0,1.5,1.5,0,0,1,0,2.121L2.121,6.07l5.009,5.009A1.5,1.5,0,0,1,6.07,13.64Z" transform="translate(3.5 5.93)"/>
                            <path id="Vector-2" data-name="Vector" d="M16.83,1.5H0A1.5,1.5,0,0,1-1.5,0,1.5,1.5,0,0,1,0-1.5H16.83A1.5,1.5,0,0,1,18.33,0,1.5,1.5,0,0,1,16.83,1.5Z" transform="translate(3.67 12)"/>
                            <path id="Vector-3" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(24 24) rotate(180)" fill="none" opacity="0"/>
                          </g>
                        </svg>
                    </a>
                </div>
                </div>
    		</div>
    	<?php } ?>	
    </div>
</div>
</div>

<?php get_footer(''); ?>