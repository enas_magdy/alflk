<?php
/**
 * Template Name: Pricinig
 * The template for displaying full-width pages with no sidebar.
 *
 * @package radix
 * @since radix 1.0
 */

get_header(); ?>


<div class="pricing-page">
    <div class="container">
        <div class="text">
            <h2>تسعير خدمات التصميم</h2>
        </div>
        <div class="desc">
            <h2>استمارة تسعير خدمات التصميم معارض تجارية</h2>
            <p>يرجى تعبئة جميع البيانات المطلوبة التى يوجد بها * بالأسفل و وضع علامة صح على الفراغات المراد إضافتها ضمن خدمات التصميم للمشروع السكني.</p>
        </div>
        <div class="wow fadeInUp">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php echo the_content(); ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>


<script>
    console.log(document.getElementById("input_10_20").value = "<?php echo $_GET['id'] ?>");

</script>


<?php get_footer(); ?>