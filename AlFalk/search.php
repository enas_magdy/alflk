<?php
/**
 * The template for displaying search results pages.
 *
 * @package radix
 * @since radix 1.0
 */


get_header(); ?>


<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php _e("search","radix"); ?></h3>
        </div>
    </div>
</div>


<div class="search-page news news-page">
    <div class="container">
        			<h2 class="search-title"><?php $allsearch = new WP_Query("s=$s&showposts=-1"); $key = esc_html($s, 1); $count = $allsearch->post_count;  echo $count . ' '; wp_reset_query(); ?><?php printf( __( ' Search Results found for' , 'radix') . '<span class="text-muted"> %s</span>', get_search_query() ); ?></h2>

        	<?php if ( have_posts() ) : ?>
        	        <div class="row">

        		<?php /* Start the Loop */ ?>
        		<?php while ( have_posts() ) : the_post(); ?>
        			<?php get_template_part( 'loop/content', 'search' ); ?>
        		<?php endwhile; ?>
        		            	<?php echo radix_pagination(); ?>

        		        </div>

        	<?php else : ?>
        		<?php get_template_part( 'loop/content', 'none' ); ?>
        	<?php endif; ?>
    </div>
</div>
     

<?php get_footer();