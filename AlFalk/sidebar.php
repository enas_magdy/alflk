<?php
/**
 * The sidebar containing the main widget area
 *
 * @package radix
 * @since radix 1.0
 *
 */
?>
<?php if ( is_active_sidebar( 'sidebar' ) ) { ?>
	<div id="sidebar" class="col-md-4 col-sm-4 col-xs-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
		<div class="sidebar-padder clearfix">
			<aside class="widget-area">
				<?php dynamic_sidebar( 'sidebar' ); ?>
			</aside>
		</div>
	</div>
<?php } ?>