<?php
/**
 * The Template for displaying all single posts.
 *
 * Please note that this is the WordPress construct of posts
 * and that other 'posts' on your WordPress site will use a
 * different template.
 *
 * @package radix
 * @since radix 1.0
 *
 */

get_header();  ?>   

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / 
            <?php  the_title(); ?> </h3>
        </div>
    </div>
</div>


 <div class="single-page">
  <div class="container">
      <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-12">
            <h3 class="main_title"> 
                <?php  the_title(); ?> 
            </h3>
            <?php while ( have_posts() ) : the_post(); ?>
            	  <?php echo do_shortcode('[gravityform id=' . get_field("service_form")['id']  .' title=false description=false ajax=true]'); ?>
            <?php endwhile; // end of the loop. ?>
            
        </div>
    </div>
   </div>
</div>


<?php 
get_footer(); ?>