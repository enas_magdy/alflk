<?php
/**
 * The Template for displaying all single posts.
 *
 * Please note that this is the WordPress construct of posts
 * and that other 'posts' on your WordPress site will use a
 * different template.
 *
 * @package radix
 * @since radix 1.0
 *
 */

get_header();  ?>   

<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> 
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php if (  'projects'  == get_post_type() ) { ?> <a href="<?php echo get_post_type_archive_link("projects"); ?>"><?php _e("Projects","radix") ?></a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php } ?> 
            <?php if (  'products'  == get_post_type() ) { ?> <a href="<?php echo get_post_type_archive_link("products"); ?>"><?php _e("Products","radix") ?></a> 
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php } ?> 
            <?php if (  'post'  == get_post_type() ) { ?> <a href="<?php echo get_page_link("2666"); ?>"><?php _e("News","radix") ?></a>
            <svg xmlns="http://www.w3.org/2000/svg" width="6.941" height="12.9" viewBox="0 0 6.941 12.9">
              <path id="chevron-left" d="M21.706,53.581l-5.479-5.718A.834.834,0,0,1,16,47.339a.76.76,0,0,1,.2-.5l5.479-5.718a.718.718,0,1,1,1.036.993l-5,5.222,5.027,5.249a.717.717,0,0,1-1.034.993Z" transform="translate(-16 -40.902)" fill="#008135"/>
            </svg>
            <?php } ?> 
            <?php  the_title(); ?> </h3>
        </div>
    </div>
</div>


 <div class="single-page">
  <div class="container">
      <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-12">
            <?php while ( have_posts() ) : the_post(); ?>
            	<?php get_template_part( 'loop/content', 'single', get_post_format() ); ?>
            	<?php if (ro_get_option('post_navigation')) { ?>
            	<?php radix_content_nav( 'nav-below' ); ?>
            	<?php } ?>
            <?php endwhile; // end of the loop. ?>
        </div>
    </div>
   </div>
</div>


<?php 
get_footer(); ?>