<?php


get_header(); ?>


<div id="breadcrumbs">
    <div class="container">
        <div class="text">
            <h3 class=""><a href="<?php echo esc_url( home_url( '/' )); ?>"><?php _e("Home"); ?> </a> / <a href="<?php echo get_page_link("2723"); ?>"> الحوكمة </a> / <?php echo get_queried_object()->name; ?></h3>
        </div>
    </div>
</div>

    <div class="container">
        <h3 class="main_title"><?php echo get_queried_object()->name; ?></h3>
        <div class="archive row no-gutters">
            <?php if ( have_posts() ) { while ( have_posts() ) : the_post(); ?>
            <div class="col-md-3 col-sm-3 col-xl-3 col-lg-3 col-12">
    <div class="item">
	    <div class="image text-center">
            <img src="<?php the_post_thumbnail_url('image');  ?>" />
        </div>
        <div class="caption">
            <h3><?php echo the_title(); ?></h3>
            <a target="_blank" href="<?php echo get_field("file");?>">
                تحميل
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="17.884" height="18.288" viewBox="0 0 17.884 18.288">
                      <g id="Group_73780" data-name="Group 73780" transform="translate(0.75 0.75)">
                        <path id="Path_105903" data-name="Path 105903" d="M14.5,8.9c3.1.267,4.365,1.86,4.365,5.346v.112c0,3.848-1.541,5.389-5.389,5.389h-5.6c-3.848,0-5.389-1.541-5.389-5.389v-.112c0-3.461,1.248-5.054,4.3-5.338" transform="translate(-2.48 -2.96)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                        <path id="Path_105904" data-name="Path 105904" d="M12,2V13.089" transform="translate(-3.804 -2)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                        <path id="Path_105905" data-name="Path 105905" d="M14.418,12.65l-2.884,2.884L8.65,12.65" transform="translate(-3.338 -3.481)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                      </g>
                    </svg>
                </span>
            </a>
        </div>
	</div>
</div>
            <?php endwhile; } ?>
        </div>
    </div>
</div>


<?php get_footer(); ?>