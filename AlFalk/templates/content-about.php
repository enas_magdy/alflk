<?php

global $about_content;
global $about_content_en;
global $about_title;
global $about_title_en;
global $about_image;
global $about_section;

if($about_section == '1') { ?>

<div class="about__section">
    <div class="container">
        <div class="about d-flex align-items-center">
            <div class="about__details">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <?php $i = 0; foreach( $about_title as $about_title_ ) { ?>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="tab-<?php echo $i; ?>" data-toggle="tab" data-target="#tab<?php echo $i; ?>" data-secondary="#tab-image-<?php echo $i; ?>" type="button" role="tab" aria-controls="home" aria-selected="true">
                                <?php if(is_rtl()) { echo $about_title[$i]; } else { echo $about_title_en[$i]; } ?>
                            </button>
                        </li>
                    <?php $i++; }  ?>
                </ul>
                <div class="tab-content" id="abouTabContent">
                    <?php $i = 0; foreach( $about_title as $about_title_ ) { ?>
                        <div class="tab-pane fade show" id="tab<?php echo $i; ?>" role="tabpanel" aria-labelledby="tab">
                            <div class="about__content">
                                <span class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Get To Know Us","radix"); ?></span>
                                <h3 class="wow fadeInRight" data-wow-delay="0.1s"><?php if(is_rtl()) { echo $about_title[$i]; } else { echo $about_title_en[$i]; } ?></h3>
                                <p><?php if(is_rtl()) { echo $about_content[$i]; } else { echo $about_content_en[$i]; } ?></p>
                                <a class="btn btn-more" href="<?php echo get_page_link("2525"); ?>"><?php _e("More Details","radix") ?></a>
                            </div>
                        </div>
                    <?php $i++; }  ?>
                </div>
            </div>
            <div class="tab-content" id="abouTabContent">
                <?php $i = 0; foreach( $about_title as $about_title_ ) { ?>
                    <div class="tab-pane fade show" id="tab-image-<?php echo $i; ?>" role="tabpanel" aria-labelledby="tab">
                        <div class="about__image">
                            <img src="<?php echo $about_image[$i]['url']; ?>" class="img-fluid" alt="<?php echo $about_title[$i]; ?>" />
                        </div>
                    </div>
                <?php $i++; }  ?>
            </div>
        </div>
    </div>
</div>

<script>
$ = jQuery;
    $(function () {
        $("ul.nav-tabs li:first-of-type button").addClass("active");
        $(".tab-content .tab-pane:first-of-type ").addClass("active");
    });
    $(document).on('click', '#myTab button', function(e) {
        otherTabs = $(this).attr('data-secondary').split(',');
          for(i= 0; i<otherTabs.length;i++) {
            nav = $('<ul class="nav d-none" id="tmpNav"></ul>');
            nav.append('<li class="nav-item"><a href="#" data-toggle="tab" data-target="' + otherTabs[i] + '">nav</a></li>"');
            nav.find('a').tab('show');
        }
    });
</script>

<?php } ?>