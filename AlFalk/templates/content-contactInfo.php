<?php 
    global $phone;
    global $address;
    global $address_en;
    global $mapaddress;
    global $mapaddress_en;
    global $email;
    global $phone_sec;

?>
<div class="contact-info">
    <div class="container">
        <div class="items d-flex align-items-center">
            <h3 class="text-center"><?php _e("Stay in touch with us", "radix"); ?></h3>
            <div class="d-flex align-items-center justify-content-center">
                <div class="item d-flex align-items-center">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="31" height="32" viewBox="0 0 31 32">
                          <defs>
                            <clipPath id="clip-path">
                              <rect id="Rectangle_65" data-name="Rectangle 65" width="31" height="32" transform="translate(0.48 -0.356)"/>
                            </clipPath>
                          </defs>
                          <g id="Group_73571" data-name="Group 73571" transform="translate(-0.48 0.356)">
                            <g id="Group_73545" data-name="Group 73545" transform="translate(0)" clip-path="url(#clip-path)">
                              <path id="Path_3679" data-name="Path 3679" d="M9.456,55.49A1.306,1.306,0,1,0,10.761,56.8,1.3,1.3,0,0,0,9.456,55.49Z" transform="translate(2.492 -45.498)"/>
                              <path id="Path_3680" data-name="Path 3680" d="M27.41,53.969C26.313,51.423,23.9,50,20.528,50H9.586A7.584,7.584,0,0,0,2,57.586V68.528C2,71.9,3.423,74.313,5.969,75.41a.662.662,0,0,0,.718-.144L27.266,54.688A.626.626,0,0,0,27.41,53.969Zm-14.272,9.4A2.617,2.617,0,0,1,11.3,64.1a2.675,2.675,0,0,1-1.841-.731c-1.332-1.254-2.794-3.251-2.233-5.628A4.066,4.066,0,0,1,11.3,54.622a4.064,4.064,0,0,1,4.074,3.134C15.919,60.119,14.457,62.117,13.138,63.371Z" transform="translate(0.611 -47.176)"/>
                              <path id="Path_3681" data-name="Path 3681" d="M23.129,70.138a.6.6,0,0,1-.1.953,8.484,8.484,0,0,1-4.178.966H7.9a.406.406,0,0,1-.274-.705l7.887-7.887a.646.646,0,0,1,.927,0Z" transform="translate(2.293 -43.119)"/>
                              <path id="Path_3682" data-name="Path 3682" d="M24.061,55.9V66.846a8.449,8.449,0,0,1-.966,4.178.6.6,0,0,1-.953.1l-6.685-6.685a.646.646,0,0,1,0-.927l7.887-7.887A.414.414,0,0,1,24.061,55.9Z" transform="translate(4.665 -45.495)"/>
                            </g>
                          </g>
                        </svg>
                    </div>    
                    <h5>
                        <span class="d-block"><?php _e("Address", "radix"); ?></span>
                        <a href="https://www.google.com.ph/maps/dir/<?php if(is_rtl()) { echo $mapaddress; } else { echo $mapaddress_en; } ?>" target="_blank">
                            <span><?php if(is_rtl()) { echo $address; } else { echo $address_en; } ?></span>
                        </a>                
                    </h5>
                </div>
                <div class="item d-flex align-items-center">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="33.992" height="34.044" viewBox="0 0 33.992 34.044">
                          <path id="Path_3694" data-name="Path 3694" d="M35.992,29.8a4.333,4.333,0,0,1-.426,1.855,7.023,7.023,0,0,1-1.157,1.736A7.674,7.674,0,0,1,31.618,35.4a8.548,8.548,0,0,1-3.319.647A14.185,14.185,0,0,1,22.75,34.8a29.9,29.9,0,0,1-5.855-3.37,48.938,48.938,0,0,1-5.583-4.766A48.365,48.365,0,0,1,6.562,21.1a30.346,30.346,0,0,1-3.336-5.8A14.337,14.337,0,0,1,2,9.728a8.806,8.806,0,0,1,.613-3.285A7.835,7.835,0,0,1,4.57,3.6,4.987,4.987,0,0,1,8.111,2a3.2,3.2,0,0,1,1.379.306,2.775,2.775,0,0,1,1.14.953l3.949,5.566a5.825,5.825,0,0,1,.681,1.192,2.69,2.69,0,0,1,.238,1.038,2.311,2.311,0,0,1-.357,1.209,5.79,5.79,0,0,1-.953,1.209l-1.294,1.345a.911.911,0,0,0-.272.681,1.545,1.545,0,0,0,.051.391c.051.136.1.238.136.34a14.125,14.125,0,0,0,1.583,2.179c.766.885,1.583,1.787,2.468,2.689.919.9,1.8,1.736,2.706,2.5a13.064,13.064,0,0,0,2.2,1.566c.085.034.187.085.306.136a1.175,1.175,0,0,0,.426.068.936.936,0,0,0,.7-.289L24.486,23.8a5.223,5.223,0,0,1,1.226-.953,2.267,2.267,0,0,1,1.209-.357,2.716,2.716,0,0,1,1.038.221,6.583,6.583,0,0,1,1.192.664l5.634,4a2.582,2.582,0,0,1,.936,1.089A3.492,3.492,0,0,1,35.992,29.8Z" transform="translate(-2 -2)"/>
                        </svg>
                    </div>
                    <h5>
                        <span class="d-block"><?php _e("Phone Number", "radix"); ?></span>
                        <a href="tel:<?php echo $phone; ?>">
                            <span><?php echo $phone; ?></span>
                        </a>
                    </h5>
                </div>
                <div class="item d-flex align-items-center">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32.138" height="27.317" viewBox="0 0 32.138 27.317">
                          <g id="Group_73549" data-name="Group 73549" transform="translate(-6186 -15465.682)">
                            <path id="Path_3685" data-name="Path 3685" d="M26.1,51.5H10.035C5.214,51.5,2,53.91,2,59.535V70.783c0,5.624,3.214,8.035,8.035,8.035H26.1c4.821,0,8.035-2.41,8.035-8.035V59.535C34.138,53.91,30.924,51.5,26.1,51.5Zm.755,9.786L21.829,65.3a6.2,6.2,0,0,1-7.52,0l-5.03-4.017a1.236,1.236,0,0,1-.193-1.7,1.2,1.2,0,0,1,1.687-.193l5.03,4.017a3.834,3.834,0,0,0,4.515,0l5.03-4.017a1.186,1.186,0,0,1,1.687.193A1.219,1.219,0,0,1,26.859,61.286Z" transform="translate(6184 15414.182)"/>
                          </g>
                        </svg>
                    </div>    
                    <h5>
                        <span class="d-block"><?php _e("E-mail", "radix"); ?></span>
                        <a href="mailto:<?php echo $email; ?>">
                            <span><?php echo $email; ?></span>
                        </a>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>