<?php 
    global $news_section;

    if($news_section) {
?>
<div class="news">
    <div class="container">
        <div class="title">
            <div class="text">
                <p class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Discover","radix"); ?></p>
                <h3 class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Latest News","radix"); ?></h3>
            </div>
            <div class="more">
        	    <a class="btn btn-more" href="<?php echo get_page_link("2666"); ?>"> <?php _e("More Details","radix") ?></a>
        	</div>
        </div>
        <div class="news-slider">
            <?php
               $query = new WP_Query( array(
                       'post_type'     => 'post', //your post type
                       'posts_per_page' => -1,
                       'order' => 'ASC',
                   )
               );
           
               while ($query->have_posts()) {
                   $query->the_post();
                   //whatever code you want
            ?>    
                <div class="item">
    			    <div class="image text-center">
                        <img src="<?php the_post_thumbnail_url('image');  ?>" />
                    </div>
                    <div class="caption">
                        <h3><?php echo the_title(); ?></h3>
                        <p><?php echo mb_strimwidth(strip_tags(get_the_content()) , 0, 80, '','utf-8'); ?></p>
                        <a class="btn btn-more" href="<?php echo get_permalink();?>">
                            <?php _e("More Details","radix"); ?>
                        </a>
                    </div>
        		</div>
        	<?php } ?>	
        </div>
    </div>
</div>

<script>
<?php if(is_rtl()) { ?>
jQuery(document).ready(function() {
    $('.news-slider').slick({
        arrows: true,
        dots: true,
        infinite: true,
         rtl: true,
        speed: 300,
        slidesToShow: 3,
        autoplay: true,
        // centerMode: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                centerMode: false,
              }
            }
        ]
    });
});
<?php } else { ?>
jQuery(document).ready(function() {
    $('.news-slider').slick({
        arrows: true,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
              }
            }
        ]
    });
});
<?php } ?>
</script>

<?php } ?>