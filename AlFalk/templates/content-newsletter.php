<?php 
    global $newsletter_section;
    global $mapaddress;
    
    if($newsletter_section) {
?>
<div class="newsletter">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-sm-7 col-md-7 col-12">
                <div class="title">
                    <div class="text">
                        <h3><?php _e("Newsletter","radix"); ?></h3>
                        <p><?php _e("More and more weekly about our news and offers","radix"); ?></p>
                    </div>
                </div>
                <div class="newsletter_form">
                    <?php echo do_shortcode('[email-subscribers-form id="1"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>



<?php if(is_rtl()){ ?>
<script>
    jQuery('.newsletter .es_txt_email').attr('placeholder', 'آدخال البريد الإلكتروني ..');
    jQuery('.newsletter input[type=submit]').val('انضم');
   
</script>
<?php } else { ?>
<script>
    jQuery('.newsletter .es_txt_email').attr('placeholder', 'enter mail');
    jQuery('.newsletter input[type=submit]').val('subscribe');
   
</script>
<?php } ?>