<?php 
    global $partners_section;

    if($partners_section) {
?>
<div class="partners_section">
    <div class="container">
        <div class="title">
            <div class="text">
                <p class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Get To Know","radix"); ?></p>
                <h3 class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Success Partners", "radix") ?></h3>
            </div>
        </div>
        <div class="partners-slider">
            <?php
               $query = new WP_Query( array(
                       'post_type'     => 'partners', //your post type
                       'posts_per_page' => -1,
                       'order' => 'ASC',
                   )
               );
           
               while ($query->have_posts()) {
                   $query->the_post();
                   //whatever code you want
            ?>    
            <div>
                <div class="item">
    			    <div class="image text-center">
                        <img src="<?php the_post_thumbnail_url('image');  ?>" />
                    </div>
        	    </div>
        	</div>    
        	<?php } ?>
        </div>
    </div>
</div>

<?php if(is_rtl()) { ?>
<script>
$ = jQuery;
    $('.partners-slider').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 1,
      rtl: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
</script>
<?php } else { ?>
<script>
$ = jQuery;
    $('.partners-slider').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
</script>
<?php } ?>

<?php } ?>