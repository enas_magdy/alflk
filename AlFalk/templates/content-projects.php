<?php 
    global $projects_section;

    if($projects_section) {
?>
<div class="projects">
    <div class="container">
        <div class="title">
            <div class="text">
                <p class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Discover","radix"); ?></p>
                <h3 class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Our Featured Projects","radix"); ?></h3>
            </div>
            <div class="more">
        	    <a class="btn btn-more" href="<?php echo get_post_type_archive_link("projects"); ?>"> <?php _e("More Details","radix") ?></a>
        	</div>
        </div>
    </div>
    <div class="projects-slider">
        <?php
           $query = new WP_Query( array(
                   'post_type'     => 'projects', //your post type
                   'posts_per_page' => -1,
                   'order' => 'ASC',
               )
           );
       
           while ($query->have_posts()) {
               $query->the_post();
               //whatever code you want
        ?>    
        <div class="item">
		    <div class="image text-center">
                <img src="<?php the_post_thumbnail_url('pro-image');  ?>" />
                <div class="icon d-flex align-items-center justify-content-center">
                    <a href="<?php echo get_permalink() ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="77.449" height="60.233" viewBox="0 0 77.449 60.233">
                          <path id="eye-regular_2_" data-name="eye-regular (2)" d="M38.743,38.453c-8.766,0-15.972,3.98-21.5,9.1A47.869,47.869,0,0,0,6.663,62.116,48.026,48.026,0,0,0,17.231,76.677c5.539,5.122,12.746,9.1,21.512,9.1s15.972-3.98,21.5-9.1A47.869,47.869,0,0,0,70.822,62.116,48.026,48.026,0,0,0,60.254,47.556C54.715,42.433,47.509,38.453,38.743,38.453ZM12.848,42.837C19.18,36.948,27.879,32,38.743,32S58.3,36.948,64.637,42.837a53.676,53.676,0,0,1,12.5,17.626,4.276,4.276,0,0,1,0,3.307A53.54,53.54,0,0,1,64.637,81.4C58.3,87.285,49.606,92.233,38.743,92.233S19.18,87.285,12.848,81.4A53.271,53.271,0,0,1,.358,63.77a4.276,4.276,0,0,1,0-3.307A53.271,53.271,0,0,1,12.848,42.837ZM38.743,72.872a10.756,10.756,0,1,0,0-21.512h-.269a8.731,8.731,0,0,1,.269,2.151,8.613,8.613,0,0,1-8.6,8.6,8.731,8.731,0,0,1-2.151-.269v.269A10.753,10.753,0,0,0,38.743,72.872Zm0-27.965A17.209,17.209,0,1,1,21.533,62.116,17.209,17.209,0,0,1,38.743,44.907Z" transform="translate(-0.025 -32)" fill="#fff"/>
                        </svg>
                        <p class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="26.72" height="35.535" viewBox="0 0 26.72 35.535">
                              <path id="location-dot-solid_1_" data-name="location-dot-solid(1)" d="M15.009,34.736C18.579,30.269,26.72,19.442,26.72,13.36A13.36,13.36,0,0,0,0,13.36c0,6.082,8.141,16.909,11.711,21.376a2.1,2.1,0,0,0,3.3,0ZM13.36,8.907A4.453,4.453,0,1,1,8.907,13.36,4.453,4.453,0,0,1,13.36,8.907Z" fill="#1d1d1b"/>
                            </svg>
                            <?php echo the_field("pro_address") ?>
                        </p>
                    </a>
                </div>
            </div>
            <div class="caption">
                <h3><?php echo the_title(); ?></h3>
                <p><?php echo mb_strimwidth(strip_tags(get_the_content()) , 0, 117, '','utf-8'); ?></p>
            </div>
		</div>
    	<?php } ?>
    	
    </div>
</div>



<script>
<?php if(is_rtl()) { ?>
jQuery(document).ready(function() {
    $('.projects-slider').slick({
        arrows: true,
        dots: true,
        infinite: false,
        rtl: true,
        speed: 300,
        slidesToShow: 3,
        rows: 2,
        autoplay: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
              }
            }
        ]
    });
});
<?php } else { ?>
jQuery(document).ready(function() {
    $('.projects-slider').slick({
        arrows: true,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
              }
            }
        ]
    });
});
<?php } ?>
</script>

<?php } ?>