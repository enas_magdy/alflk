<?php

global $statistics_section;
global $statistics_title;
global $statistics_content;
global $statistics_title_en;
global $statistics_img;
global $statistics_image;


if($statistics_section == '1') { ?>

<div class="statistics__section">
    <div class="d-flex no-gutters align-items-center">
        <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
            <div class="title">
                <div class="text">
                    <p class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Get To Know","radix"); ?></p>
                    <h3 class="wow fadeInRight" data-wow-delay="0.1s"><?php _e("Our Numbers","radix"); ?></h3>
                </div>
            </div>
            <div class="statistics-items d-flex justify-content-between align-items-center">
                <?php $i = 0;foreach($statistics_title as $statistics_title_) { ?>
                    <div class="item d-flex flex-column align-items-center justify-content-center">
                        <div class="image">
                            <img src="<?php echo $statistics_img[$i]['url']; ?>" class="img-fluid" />
                        </div>
                        <div class="text">
                            <h2 class="number"><?php echo $statistics_content[$i]; ?></h2>
                            <p class="title"><?php if(is_rtl()) { echo $statistics_title[$i]; } else { echo $statistics_title_en[$i]; } ?></p>
                        </div>
                    </div>
                <?php $i++; } ?>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
            <div class="image">
                <img src="<?php echo $statistics_image['url']; ?>" class="img-fluid" />
            </div>
        </div>
    </div>
</div>

<?php } ?>