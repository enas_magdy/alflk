<?php 
    global $vision_section;
    global $vision_content;
    global $vision_content_en;
    global $vision_img;
    
    if($vision_section) {
?>
<div class="vision">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-sm-8 col-md-7 col-12">
                <div class="title">
                    <div class="text">
                        <h3><?php _e("Our vision ... a homeland","radix"); ?></h3>
                        <p><?php if(is_rtl()) { echo $vision_content; } else { echo $vision_content_en; }  ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-12">
                <div class="image">
                    <img src="<?php echo $vision_img['url']; ?>" />
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>